
.. index::
   ! Glossaire


.. _glossaire:

================
Glossaire
================

.. seealso::

   - https://docs.anaconda.com/anaconda/glossary

.. glossary::


   Agile Software Development
       A set of concepts, practices and principles for the development of
       software under which both requirements and the software that meets
       them evolve during the development life-cycle by processes of
       collaboration, as opposed to being defined at milestones within it

   Containers
       Running instances of Docker images — containers run the actual
       applications.
       A container includes an application and all of its dependencies.
       It shares the kernel with other containers, and runs as an
       isolated process in user space on the host OS.
       You created a container using docker run which you did using
       the alpine image that you downloaded.
       A list of running containers can be seen using the docker ps command.


   Conteneur
       Un conteneur est une instance d'une image qui peut être créée,
       démarrée et stoppée.

       (Source: Hackable, mars/avril 2022)

   Docker
       Définition 1 (anglais)
           Docker allows us to easily create clean, pre-installed images
           of our application in an isolated state, like a binary application
           build, rather than having to worry about virtual environments
           and system packages of whatever server we are deploying to.
           This build can then be tested and deployed as if it was an
           isolated artifact in and of itself.

           Source: https://peakwinter.net/blog/modern-devops-django/

       Définition 2
           With Docker, you can run your Django project on an Ubuntu
           server in a container on your laptop, and because Docker is
           available for Mac, Linux, and Windows, your choice of
           operating system really comes down to preference.
           When it comes time to push your code to a staging or
           production server, you can be sure it’ll run exactly
           the same as it did on your laptop, because you can configure
           a Dockerfileto exactly match these environments.

           Source: https://medium.com/@adamzeitcode/a-simple-recipe-for-django-development-in-docker-bonus-testing-with-selenium-6a038ec19ba5



   Docker daemon
       The background service running on the host that manages building,
       running and distributing Docker containers.

   Docker client
       The command line tool that allows the user to interact with
       the Docker daemon.

   docker-compose.yml
        Definition 1 (français)
            Le docker compose est un fichier de configuration de l’ensemble
            des Dockers que vous souhaitez déployer pour votre application,
            il sert à les déployer et à gérer les liens entre les conteneurs
            ainsi que les volumes de data.

        Definition 2 (anglais)
            The file where you can set up your database, automatically start
            your server when you start your container, and cool stuff like
            that.

            Source: https://www.revsys.com/tidbits/brief-intro-docker-djangonauts/


        Définition 3 (anglais)
            Docker Compose lets you run more than one container in a
            Docker application.
            It’s especially useful if you want to have a database, like
            Postgres, running in a container alongside your web app.
            (Docker’s overview of Compose is helpful.)
            Compose allows you to define several services that will
            make up your app and run them all together.

            Source: https://www.revsys.com/tidbits/brief-intro-docker-djangonauts/



   Dockerfile
       Definition 1 (français)
           Un Dockerfile est une description permettant de créer une
           image du système qui sera utilisé dans chaque conteneur.

           (Source: Hackable, mars/avril 2022)

       Definition 2 (franglais)
           C’est le fichier texte qui décrit la configuration de votre docker.
           En général, on part d’une image standard et on ajoute les
           éléments propres à la configuration de l’application que l’on
           veut déployer ; une fois le Dockerfile finalisé, on *build*
           le conteneur.

       Definition 3 (anglais)
           The name of the file that contains the instructions for setting
           up your image.
           Source: https://www.revsys.com/tidbits/brief-intro-docker-djangonauts/


   image
   image docker
       Definition 1 (français)
           Une image est une représentation du système tel qu'il prendra
           place dans un conteneur;
           Voyez cela comme un modèle, un template ou une classe qui sera
           instanciée;

           (Source: Hackable, mars/avril 2022)

       Definition 2 (français)
           C’est l’élément de base d’un docker, on utilise une Docker image
           à deux stades :

           - Au départ, on va chercher une image de base standard pour
             l’applicatif choisi (Nginx, Php, Redis), le plus souvent dans
             un repository public, on complète ensuite cette image standard
             des éléments de configuration de votre application, vous pouvez
             ensuite enregistrer la nouvelle image dans un repository
             public, ou privé.

       Definition 3 (anglais)
           The file system and configuration of our application which are
           used to create containers.
           To find out more about a Docker image, run::

               docker inspect alpine

           In the demo above, you used the docker pull command to download
           the alpine image.
           When you executed the command docker run hello-world, it also did
           a docker pull behind the scenes to download the hello-world image.

       Definition 3 (anglais)
           A **lightweight, stand-alone, executable package that includes
           everything needed to run a piece of software.**
           You will set up a specific image for each project you work on
           that will tell Docker which packages your project needs,
           where your code lives, etc.

           Source: https://www.revsys.com/tidbits/brief-intro-docker-djangonauts/



   Docker Store
       A registry of Docker images, where you can find trusted and
       enterprise ready containers, plugins, and Docker editions.
       You'll be using this later in this tutorial.

   hyperviseur
   Hyperviseur
       En informatique, un hyperviseur est une plate-forme de virtualisation
       qui permet à plusieurs systèmes d'exploitation de travailler sur
       une même machine physique en même temps.

   Hyper-V
       Microsoft Hyper-V, codenamed Viridian and formerly known as
       Windows Server Virtualization, is a native hypervisor; it can create
       virtual machines on x86-64 systems running Windows.

       Hyper-V, également connu sous le nom de Windows Server Virtualisation,
       est un système de virtualisation basé sur un hyperviseur 64 bits de la
       version de Windows Server 2008.


   Orchestrateur de conteneurs
       L’orchestrateur est un peu au conteneur ce que vSphere/vCenter
       est à VMware pour des VMs, c’est le logiciel de gestion de
       l’ensemble des conteneurs sur un pool de ressources serveurs,
       avec davantage de fonctionnalités que vSphere_/vCenter.
       C’est en quelque sorte un PaaS_ pour les conteneurs

       .. _PaaS: https://fr.wikipedia.org/wiki/Plate-forme_en_tant_que_service

       .. _vSphere: https://fr.wikipedia.org/wiki/VMware_vSphere

       .. figure:: schema-archi-conteneur-Docker.png
          :align: center

          Un orchestrateur gère un pool de ressources serveurs et réseau
          .. seealso:: https://actu.alfa-safety.fr/devops/docker-en-production/

   reverse proxy
   proxy inverse
       Un proxy inverse (reverse proxy) est un type de serveur, habituellement
       placé en frontal de serveurs web.
       Contrairement au serveur proxy qui permet à un utilisateur d'accéder
       au réseau Internet, le proxy inverse permet à un utilisateur d'Internet
       d'accéder à des serveurs internes, une des applications courantes
       du proxy inverse est la répartition de charge (load-balancing).

       Le proxy inverse est installé du côté des serveurs Internet.
       L'utilisateur du Web passe par son intermédiaire pour accéder aux
       applications de serveurs internes.
       Le proxy inverse est parfois appelé substitut (surrogate)

       .. seealso:: https://fr.wikipedia.org/wiki/Proxy_inverse


   Essaim
   Swarm
   swarm
       A swarm is a cluster of one or more Docker Engines running
       in swarm mode.
       En français *swarm* est un essaim

       .. figure:: ../images/docker-swarm.gif
          :align: center

          Un essaim de docker engines

   Virtual machine
       Have you ever seen someone boot up Windows on a Mac ?
       That process of running one complete OS on top of another OS called
       running a **virtual machine**.

       .. seealso:: http://erick.matsen.org/2018/04/19/docker.html
