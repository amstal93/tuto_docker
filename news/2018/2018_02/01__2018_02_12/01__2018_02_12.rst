
.. index::
   pair: Action ; 2018-02-12
   pair: HeidiSQL ; PostgreSQL


.. _01__2018_02_12:

=============================================================================
Lundi 12 février 2018: mise en place d'une base de données PostgreSQL 10.2
=============================================================================




Dockerfile
===========

::

	# https://store.docker.com/images/postgres
	FROM postgres:10.2
	# avec cette image on peut mettre en place la locale fr_FR.utf8
	RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8
	ENV LANG fr_FR.utf8


docker-compose.yml
===================

::

	version: "3"
	services:
	  db:
		build:
		  context: .
		  dockerfile: Dockerfile
		ports:
		  # the 5432 host port is occupied by a local postgressql server
		  - 5433:5432
		volumes:
		  - volume_intranet:/var/lib/postgresql/data/

	volumes:
	  volume_intranet:



Accès HeidiSQL à partir de la machine hôte
===========================================

.. figure:: acces_heidisql.png
   :align: center

   Accès HeidiSQL à partir de la machine hôte sur le port 5433
