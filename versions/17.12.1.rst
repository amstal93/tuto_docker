

.. index::
   pair: docker-ce ; 17.12.1-ce (2018-02-27)

.. _docker_17_12_1:

===================================
17.12.1-ce (2018-02-27)
===================================

.. seealso::

   - https://github.com/docker/docker-ce/tree/v17.12.1-ce
