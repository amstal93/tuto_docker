

.. index::
   pair: docker-ce ; 17.06.0-ce (2017-06-23, 02c1d87)

.. _docker_17_06:

===================================
17.06.0-ce (2017-06-23, 02c1d87)
===================================

.. seealso::

   - https://github.com/docker/docker-ce/tree/v17.06.0-ce
