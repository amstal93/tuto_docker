
.. index::
   pair: docker-compose ; V2
   ! docker-compose V2

.. _docker_compose_V2:

=============================================
**docker-compose V2**
=============================================

- :ref:`docker_compose_2_0_0`
- https://docs.docker.com/compose/cli-command/


Transitioning to GA (Generally Available) for Compose V2
==============================================================

We are currently working towards providing a standard way to install
Compose V2 on Linux.

When this is available, Compose V2 will be marked as Generally Available (GA).

Compose V2 GA means:

New features and bug fixes will only be considered in the Compose V2 code base.
Docker Compose V2 will be the default setting in Docker Desktop for Mac
and Windows. You can still opt out through the Docker Desktop UI and the CLI.

This means, when you run docker-compose, you will actually be running
docker compose.

Compose V2 will be included with the latest version of the Docker CLI.

You can use Compose Switch to redirect docker-compose to docker compose.

Compose V2 branch will become the default branch.

Docker Compose V1 will be maintained to address any security issues.

Important
=============

We would like to make the Compose V2 transition to be as smooth as possible
for all users.
We currently don’t have a concrete timeline to deprecate Compose V1.

We will review the feedback from the community on the GA and the adoption
on Linux, and come up with a plan to deprecate Compose V1.

We are not planning to remove the aliasing of docker-compose to docker compose.

We would like to make it easier for users to switch to V2 without breaking
any existing scripts.
We will follow up with a blog post with more information on the exact
timeline on V1 deprecation and the end of support policies for security issues.

Your feedback is important to us. Reach out to us and let us know your
feedback on our Public Roadmap.


Install on Linux
=========================

You can install Compose V2 by downloading the appropriate binary for
your system from the project release page and copying it into $HOME/.docker/cli-plugins
as docker-compose.

Run the following command to download the current stable release of
Docker Compose::

    DOCKER_CONFIG=${DOCKER_CONFIG:-$HOME/.docker}
    mkdir -p $DOCKER_CONFIG/cli-plugins
    curl -SL https://github.com/docker/compose/releases/download/v2.2.3/docker-compose-linux-x86_64 -o $DOCKER_CONFIG/cli-plugins/docker-compose

This command installs Compose V2 for the active user under $HOME directory.

To install Docker Compose for all users on your system,
replace ~/.docker/cli-plugins with /usr/local/lib/docker/cli-plugins.

Apply executable permissions to the binary::

    chmod +x $DOCKER_CONFIG/cli-plugins/docker-compose

Test your installation

::

    docker compose version


[Docker Compose] V1 End of Life Policy #257
===============================================

- https://github.com/docker/roadmap/issues/257


Transition to use Compose v2 in moodle-docker-compose wrapper #202
====================================================================

 - https://github.com/moodlehq/moodle-docker/issues/202#issue-1125310678
