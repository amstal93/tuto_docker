

.. index::
   pair: Tutoriel ; Play with Docker


.. _play_with_docker:

===========================================
play with docker
===========================================


.. seealso::

   - https://training.play-with-docker.com/






Docker for IT Pros and System Administrators Stage 1
======================================================

.. seealso::

   - https://training.play-with-docker.com/ops-stage1/



Docker for Beginners - Linux
==============================


.. seealso::

   - https://training.play-with-docker.com/beginner-linux/
