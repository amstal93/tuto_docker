
.. index::
   pair: Docker ; MISC 95



.. _misc_95:

=======================================================================================
**Docker: les bons réflexes à adopter** par Paul MARS (MISC 95)
=======================================================================================


.. seealso::

   - https://www.miscmag.com/misc-n95-references-de-larticle-docker-les-bons-reflexes-a-adopter/
   - https://www.miscmag.com/misc-n95-references-de-larticle-apercu-de-la-securite-de-docker/





.. _dockerfile_misc95:

Dockerfile MISC 95
==========================

::

    FROM python:2.7-alpine  # usage d'une image de base du dépôt officiel
	LABEL description "Internal info on the challenge" version "0.1" # ajout
	d'informations à l'image pour pouvoir l'identifier plus facilement
	WORKDIR /opt/app/  # définition d'un dossier de travail pour l'exécution
	des instructions suivantes
	RUN addgroup -S ndh && adduser -S -g ndh ndh  # exécution d'une commande
	dans l'image
	USER ndh
	COPY requirements.txt /opt/app/  # copie de plusieurs ressources depuis
	l'hôte vers l'image
	COPY flag.txt /etc/x.b64
    RUN pip install -r requirements.txt
	RUN rm requirements.txt
	COPY wsgi.py /opt/app/
	COPY cmd.sh /opt/app/
	COPY xml_challenge /opt/app/xml_challenge
	EXPOSE 8002  # définition de la liste des ports que les conteneurs
	instanciés sur l'image pourraient exposer
	CMD [ "/bin/sh", "cmd.sh" ] # définition de la commande qui sera
	lancée à l'instanciation d'un conteneur à partir de l'image


Vous aurez noté la présence d'une directive USER dans le
Dockerfile précédent ainsi que de la création d'un utilisateur *ndh*
quelques lignes plus haut.
Par défaut, un processus lancé dans un conteneur s'exécute en tant que
**root**. Vous vous doutez que ce comportement par défaut n'est pas
une bonne pratique. Docker propose la directive USER permettant
d'opérer le changement d'utilisateur.

Il faut simplement l'avoir créé avant dans le Dockerfile ou qu'il soit
présent dans l'image sur laquelle se base la vôtre.
Toutes les commandes exécutées au sein de l'image et du
conteneur instancié sur cette image seront effectuées avec cet
utilisateur après la directive.
Pour chacun des services, il a été créé un utilisateur **ndh** dont les
droits ont été modulés en fonction des besoins (besoin d'un shell ou non,
droits sur certains fichiers).
En pratique, cela a permis de donner un shell aux utilisateurs afin
qu'ils récupèrent un drapeau sur le serveur sans qu'ils puissent le
modifier ou changer l'environnement d'exécution du service.



La présence de secrets dans un Dockerfile ou un fichier docker-compose.yml.


Fichiers .env
================


Ces fichiers sont destinés à être versionnés et manipulés par plusieurs
équipes.
Docker dispose de fonctionnalités de gestion des secrets à travers la
commande docker secrets (vous vous en doutiez, n'est-ce pas ?).

En parallèle de cette commande, une bonne pratique est de gérer les
secrets par variable d'environnement et de passer ces variables à
l'instanciation via la lecture d'un fichier de configuration.
