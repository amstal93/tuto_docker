===========
README.txt
===========

Construire l'image
====================

::

    docker build -t id3pipenvpb:0.1.1 .
    docker build -t id3pipenvpb:0.1.2 .

Lancer le conteneur
====================

::

    docker run --name id3pipenvpb.0.1.1 -it id3pipenvpb:0.1.1
    docker run --name id3pipenvpb.0.1.2 -it id3pipenvpb:0.1.2

Accéder au conteneur
======================

::

    docker exec -it id3pipenvpb.0.1.1 bash



Arrêter un conteneur
=======================

::

    docker stop id3pipenvpb.0.1.1
