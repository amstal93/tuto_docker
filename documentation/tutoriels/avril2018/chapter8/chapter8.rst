

.. index::
   pair: Chapter8 ; Avril 2018


.. _chapter8_avril_2018:

====================================================
Chapter8 Avril 2018
====================================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#583
   - https://avril2018.container.training/intro.yml.html#14
   - :ref:`petazzoni`


.. toctree::
   :maxdepth: 4
