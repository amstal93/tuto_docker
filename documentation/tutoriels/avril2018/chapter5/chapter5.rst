

.. index::
   pair: Chapter5 ; Avril 2018


.. _chapter5_avril_2018:

====================================================
Chapter5 Avril 2018
====================================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#335
   - https://avril2018.container.training/intro.yml.html#11
   - :ref:`petazzoni`


.. toctree::
   :maxdepth: 4

   local_dev/local_dev
   volumes/volumes
   compose/compose
   docker_machine/docker_machine
