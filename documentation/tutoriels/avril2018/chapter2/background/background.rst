

.. index::
   pair: background ; container
   pair: running ; containers
   pair: IDs ; containers
   pair: logs ; container



.. _background_containers:

======================
Backgroud containers
======================




Objectives
==============

Our first containers were interactive.

We will now see how to:

- Run a non-interactive container.
- Run a container in the background.
- List running containers.
- Check the logs of a container.
- Stop a container.
- List stopped containers.


.. _non_interative_container:

A non-interactive container
==============================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#97

::

    $ docker run jpetazzo/clock


::

	Unable to find image 'jpetazzo/clock:latest' locally
	latest: Pulling from jpetazzo/clock
	a3ed95caeb02: Pull complete
	1db09adb5ddd: Pull complete
	Digest: sha256:446edaa1594798d89ee2a93f660161b265db91b026491e4671c14371eff5eea0
	Status: Downloaded newer image for jpetazzo/clock:latest
	Wed May 30 08:34:23 UTC 2018
	Wed May 30 08:34:24 UTC 2018
	Wed May 30 08:34:25 UTC 2018
	Wed May 30 08:34:26 UTC 2018
	Wed May 30 08:34:27 UTC 2018
	Wed May 30 08:34:28 UTC 2018
	Wed May 30 08:34:29 UTC 2018


.. _container_backgroud:

Run a container in the background
====================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#98


Containers can be started in the background, with the -d flag (daemon mode)

::

    $ docker run -d jpetazzo/clock

::

    36935b2a967fd69c7fa23788e00855baa1896cc4af111fb78b9cfcc70a4d409c



- We don't see the output of the container.
- But don't worry: Docker collects that output and logs it!
- Docker gives us the ID of the container.


.. _list_running_containers:

List running containers
===========================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#99
   - :ref:`docker_ps`

::

    $ docker ps

::

	CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
	36935b2a967f        jpetazzo/clock      "/bin/sh -c 'while d…"   3 minutes ago       Up 3 minutes                            lucid_kapitsa


Docker tells us:

- The (truncated) ID of our container.
- The image used to start the container.
- That our container has been running (Up) for a couple of minutes.
- Other information (COMMAND, PORTS, NAMES) that we will explain later


.. _view_ids:

View only the IDs of the containers
=====================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#102
   - :ref:`docker_ps`


Many Docker commands will work on container IDs: docker stop, docker rm...

If we want to list only the IDs of our containers (without the other
colums or the header line), we can use the -q ("Quiet", "Quick") flag:

::

    docker ps -q


Combining flags
=================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#102
   - :ref:`docker_ps`



We can combine -l and -q to see only the ID of the last container started:

::

    $ docker ps -lq

::

    3c181d8dbc4c

At a first glance, it looks like this would be particularly useful in
scripts.

However, if we want to start a container and get its ID in a reliable way,
it is better to use docker run -d, which we will cover in a bit.


.. _view_logs:

View the logs of a container
===============================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#104
   - :ref:`docker_logs`


We told you that Docker was logging the container output.

Let's see that now.

::

    docker logs 3c18


::

	Wed May 30 08:49:15 UTC 2018
	Wed May 30 08:49:16 UTC 2018
	Wed May 30 08:49:17 UTC 2018
	Wed May 30 08:49:18 UTC 2018
	Wed May 30 08:49:19 UTC 2018
	Wed May 30 08:49:20 UTC 2018
	Wed May 30 08:49:21 UTC 2018
	Wed May 30 08:49:22 UTC 2018
	Wed May 30 08:49:23 UTC 2018
	Wed May 30 08:49:24 UTC 2018
	Wed May 30 08:49:25 UTC 2018
	Wed May 30 08:49:26 UTC 2018
	Wed May 30 08:49:27 UTC 2018


View only the tail of the logs
==================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#104
   - :ref:`docker_logs`


To avoid being spammed with eleventy pages of output, we can use the
--tail option::

    $ docker logs 3c18 --tail 3

::

	Wed May 30 09:02:29 UTC 2018
	Wed May 30 09:02:30 UTC 2018
	Wed May 30 09:02:31 UTC 2018

The parameter is the number of lines that we want to see.


.. _follow_logs:

Follow the logs in real time
===============================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#106
   - :ref:`docker_logs`


Just like with the standard UNIX command tail -f, we can follow the
logs of our container:

::

    $ docker logs --follow 3c18 --tail 1

::

	Wed May 30 09:07:11 UTC 2018
	Wed May 30 09:07:12 UTC 2018
	Wed May 30 09:07:13 UTC 2018
	Wed May 30 09:07:14 UTC 2018
	Wed May 30 09:07:15 UTC 2018


- This will display the last line in the log file.
- Then, it will continue to display the logs in real time.
- Use ^C to exit.


.. _container_stop:

Stop our container
=====================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#107
   - :ref:`docker_stop`


There are two ways we can terminate our detached container.

- Killing it using the docker kill command.
- Stopping it using the docker stop command.

The first one stops the container immediately, by using the KILL signal.

The second one is more graceful. It sends a TERM signal, and after
10 seconds, if the container has not stopped, it sends KILL.


Reminder: the KILL signal cannot be intercepted, and will forcibly
terminate the container.


Stopping our containers
===========================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#108
   - :ref:`docker_stop`


Let's stop one of those containers:

::

    $ docker stop 3c1

::

    3c1

This will take 10 seconds:

- Docker sends the TERM signal;
- the container doesn't react to this signal (it's a simple Shell script
  with no special signal handling);
- 10 seconds later, since the container is still running, Docker sends
  the KILL signal;
- this terminates the container.


Killing the remaining containers
===================================

.. seealso::

   - https://avril2018.container.training/intro.yml.html#109
   - :ref:`docker_stop`


Let's be less patient with the two other containers:

::

    $ docker stop 1fe 369

::

	1fe
	369

The stop and kill commands can take multiple container IDs.

Those containers will be terminated immediately (without the 10 seconds delay).

Let's check that our containers don't show up anymore::

    $ docker ps

::

    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
