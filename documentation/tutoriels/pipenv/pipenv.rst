

.. index::
   pair: Tutoriel  ; Pipenv avec Docker


.. _pipenv_docker:

===========================================
Tutoriel Utilisation de pipenv avec Docker
===========================================


.. seealso::

   - https://github.com/dfederschmidt/docker-pipenv-sample
   - https://github.com/pypa/pipenv/blob/master/Dockerfile





Les fichiers
=============

::

    Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\pipenv>dir

::

	Le volume dans le lecteur Y n’a pas de nom.
	Le numéro de série du volume est B2B7-2241

	Répertoire de Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\pipenv

	22/01/2018  10:39    <DIR>          .
	22/01/2018  10:39    <DIR>          ..
	22/01/2018  08:23               250 app.py
	22/01/2018  10:11               438 Dockerfile
	22/01/2018  10:39             8 130 pipenv.rst
	22/01/2018  08:23               129 Pipfile
	22/01/2018  08:23             2 580 Pipfile.lock
	22/01/2018  08:23               415 Readme.md
	6 fichier(s)           11 942 octets
	2 Rép(s)  20 168 241 152 octets libres





Réécriture du fichier Dockerfile
==================================

.. seealso::

   - https://github.com/pypa/pipenv/blob/master/Dockerfile


On part de la `recommendation officielle de Kenneth Reitz`_.


.. _`recommendation officielle de Kenneth Reitz`:  https://github.com/pypa/pipenv/blob/master/Dockerfile


.. literalinclude:: Dockerfile
   :linenos:


app.py
========


.. literalinclude:: app.py
   :linenos:



docker build -t docker-pipenv-sample . : construction de l'image
===================================================================

::

    C:/projects_id3/docker_projects/docker-pipenv-sample>docker build -t docker-pipenv-sample .

::

	Sending build context to Docker daemon  78.34kB
	Step 1/8 : FROM python:3.6
	3.6: Pulling from library/python
	Digest: sha256:98149ed5f37f48ea3fad26ae6c0042dd2b08228d58edc95ef0fce35f1b3d9e9f
	Status: Downloaded newer image for python:3.6
	 ---> c1e459c00dc3
	Step 2/8 : RUN set -ex && pip install pipenv --upgrade
	 ---> Running in 21e4931d7ee4
	+ pip install pipenv --upgrade
	Collecting pipenv
	  Downloading pipenv-9.0.3.tar.gz (3.9MB)
	Collecting virtualenv (from pipenv)
	  Downloading virtualenv-15.1.0-py2.py3-none-any.whl (1.8MB)
	Collecting pew>=0.1.26 (from pipenv)
	  Downloading pew-1.1.2-py2.py3-none-any.whl
	Requirement already up-to-date: pip>=9.0.1 in /usr/local/lib/python3.6/site-packages (from pipenv)
	Collecting requests>2.18.0 (from pipenv)
	  Downloading requests-2.18.4-py2.py3-none-any.whl (88kB)
	Collecting flake8>=3.0.0 (from pipenv)
	  Downloading flake8-3.5.0-py2.py3-none-any.whl (69kB)
	Collecting urllib3>=1.21.1 (from pipenv)
	  Downloading urllib3-1.22-py2.py3-none-any.whl (132kB)
	Collecting virtualenv-clone>=0.2.5 (from pew>=0.1.26->pipenv)
	  Downloading virtualenv-clone-0.2.6.tar.gz
	Collecting setuptools>=17.1 (from pew>=0.1.26->pipenv)
	  Downloading setuptools-38.4.0-py2.py3-none-any.whl (489kB)
	Collecting certifi>=2017.4.17 (from requests>2.18.0->pipenv)
	  Downloading certifi-2018.1.18-py2.py3-none-any.whl (151kB)
	Collecting chardet<3.1.0,>=3.0.2 (from requests>2.18.0->pipenv)
	  Downloading chardet-3.0.4-py2.py3-none-any.whl (133kB)
	Collecting idna<2.7,>=2.5 (from requests>2.18.0->pipenv)
	  Downloading idna-2.6-py2.py3-none-any.whl (56kB)
	Collecting mccabe<0.7.0,>=0.6.0 (from flake8>=3.0.0->pipenv)
	  Downloading mccabe-0.6.1-py2.py3-none-any.whl
	Collecting pycodestyle<2.4.0,>=2.0.0 (from flake8>=3.0.0->pipenv)
	  Downloading pycodestyle-2.3.1-py2.py3-none-any.whl (45kB)
	Collecting pyflakes<1.7.0,>=1.5.0 (from flake8>=3.0.0->pipenv)
	  Downloading pyflakes-1.6.0-py2.py3-none-any.whl (227kB)
	Building wheels for collected packages: pipenv, virtualenv-clone
	  Running setup.py bdist_wheel for pipenv: started
	  Running setup.py bdist_wheel for pipenv: finished with status 'done'
	  Stored in directory: /root/.cache/pip/wheels/78/cf/b7/549d89ddbafb1cf3da825b97b730a7e1ac75602de9865d036e
	  Running setup.py bdist_wheel for virtualenv-clone: started
	  Running setup.py bdist_wheel for virtualenv-clone: finished with status 'done'
	  Stored in directory: /root/.cache/pip/wheels/24/51/ef/93120d304d240b4b6c2066454250a1626e04f73d34417b956d
	Successfully built pipenv virtualenv-clone
	Installing collected packages: virtualenv, virtualenv-clone, setuptools, pew, urllib3, certifi, chardet, idna, requests, mccabe, pycodestyle, pyflakes, flake8, pipenv
	  Found existing installation: setuptools 38.2.4
		Uninstalling setuptools-38.2.4:
		  Successfully uninstalled setuptools-38.2.4
	Successfully installed certifi-2018.1.18 chardet-3.0.4 flake8-3.5.0 idna-2.6 mccabe-0.6.1 pew-1.1.2 pipenv-9.0.3 pycodestyle-2.3.1 pyflakes-1.6.0 requests-2.18.4 setuptools-38.4.0 urllib3-1.22 virtualenv-15.1.0 virtualenv-clone-0.2.6
	Removing intermediate container 21e4931d7ee4
	 ---> 0b1272e6e1c6
	Step 3/8 : RUN set -ex && mkdir /app
	 ---> Running in 21153ac29a7f
	+ mkdir /app
	Removing intermediate container 21153ac29a7f
	 ---> 1f95b3a89e78
	Step 4/8 : WORKDIR /app
	Removing intermediate container d235da053693
	 ---> c40c0a57be56
	Step 5/8 : COPY Pipfile Pipfile
	 ---> 72c20255a55d
	Step 6/8 : COPY Pipfile.lock Pipfile.lock
	 ---> 7f022488626e
	Step 7/8 : RUN set -ex && pipenv install --deploy --system
	 ---> Running in 7535ac2a9610
	+ pipenv install --deploy --system
	Installing dependencies from Pipfile.lock (d3d473)…
	Removing intermediate container 7535ac2a9610
	 ---> 7366de78a2f1
	Step 8/8 : COPY . /app
	 ---> 5c977e084023
	Successfully built 5c977e084023
	Successfully tagged docker-pipenv-sample:latest
	SECURITY WARNING: You are building a Docker image from Windows against a non-Windows Docker host.
	All files and directories added to build context will have '-rwxr-xr-x' permissions.
	It is recommended to double check and reset permissions for sensitive files and directories.



docker run -p 5000:5000 docker-pipenv-sample
==============================================

::

    C:/projects_id3/docker_projects/docker-pipenv-sample>docker run -p 5000:5000 docker-pipenv-sample


::

	 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
	 * Restarting with stat
	 * Debugger is active!
	 * Debugger PIN: 153-767-505


http://localhost:5000/
==========================


docker ps
==============

::

    Y:/projects_id3/P5N001/XLOGCA135_tutorial_docker/tutorial_docker/tutoriels/pipenv>docker ps

::

	CONTAINER ID        IMAGE                  COMMAND             CREATED             STATUS              PORTS                    NAMES
	b9bf3fbbb859        docker-pipenv-sample   "python app.py"     4 minutes ago       Up 4 minutes        0.0.0.0:5000->5000/tcp   condescending_hypatia


docker exec -it 1a0a3dc7924d bash
==================================

::

    Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\pipenv>docker exec -it b9bf3fbbb859 bash

::

    root@b9bf3fbbb859:/app# ls -als

::

	4 drwxr-xr-x 1 root root 4096 Jan 22 09:44 .
	4 drwxr-xr-x 1 root root 4096 Jan 22 09:45 ..
	4 -rwxr-xr-x 1 root root  129 Jan 22 07:23 Pipfile
	4 -rwxr-xr-x 1 root root 2580 Jan 22 07:23 Pipfile.lock
	4 -rwxr-xr-x 1 root root  248 Jan 22 09:43 app.py


::

    root@1a0a3dc7924d:/app# ps -ef | grep python

::

	root          1      0  0 08:42 ?        00:00:00 python app.py
	root          7      1  0 08:42 ?        00:00:10 /usr/local/bin/python app.py


docker rm 1a0a3dc7924d: suppression du conteneur à l'arrêt
============================================================

::

    Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\pipenv>docker rm 1a0a3dc7924d

::

    1a0a3dc7924d


docker rmi docker-pipenv-sample: suppression de l'image
==========================================================

::

    Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\pipenv>docker rmi docker-pipenv-sample

::

	Untagged: docker-pipenv-sample:latest
	Deleted: sha256:f7cb7fa32f377aa356791f7149f8f21b2b668e6ce5011dc338cb8ea7c58778b9
	Deleted: sha256:91953983b1e474e3aff636101c4625d825c8a54044a7a44026d8a4a049efa5d7
	Deleted: sha256:b08673d3c06b5d6c576e64d0c87f1d09d53355ae8f416d9e12b125bb78425721
