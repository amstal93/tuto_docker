
.. index::
   pair: Windows ; Docker


.. _tutos_docker_windows:

===============================
Tutoriels Docker pour Windows
===============================

.. seealso::

   - https://docs.docker.com/docker-for-windows/





Installation
===============

.. seealso::

   - https://docs.docker.com/docker-for-windows/install/
   - https://download.docker.com/win/stable/Docker%20for%20Windows%20Installer.exe
   - https://nickjanetakis.com/blog/setting-up-docker-for-windows-and-wsl-to-work-flawlessly


Installation de "Docker for windows" quand on a une machine sous
Windows 10.


.. figure:: docker_on_windows.png
   :align: center



docker --version
=================

::

    docker --version

::

    Docker version 17.12.0-ce, build c97c6d6



docker-compose --version
==========================

::

    Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker>docker-compose --version


::

    docker-compose version 1.18.0, build 8dd22a96


docker-machine --version
===========================

::

    Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker>docker-machine --version

::

    docker-machine version 0.13.0, build 9ba6da9


notary version
===============

::

    Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker>notary version

::

	notary
	 Version:    0.4.3
	 Git commit: 9211198


Binaires docker sous Windows 10
=================================

.. figure:: binaires_windows.png
   :align: center

   Binaires docker sous Windows 10



Where to go next
==================

.. seealso::

   - https://docs.docker.com/get-started/
   - https://github.com/docker/labs/
   - https://docs.docker.com/engine/reference/commandline/docker/
   - https://blog.docker.com/2017/01/whats-new-in-docker-1-13/


- Try out the walkthrough at `Get Started`_.
- Dig in deeper with `Docker Labs`_ example walkthroughs and source code.
- For a summary of Docker command line interface (CLI) commands, see the `Docker CLI Reference Guide`_.
- Check out the blog post `Introducing Docker 1.13.0`_.


.. _`Get Started`:  https://docs.docker.com/get-started/
.. _`Docker Labs`: https://github.com/docker/labs/
.. _`Docker CLI Reference Guide`: https://docs.docker.com/engine/reference/commandline/docker/
.. _`Introducing Docker 1.13.0`:   https://blog.docker.com/2017/01/whats-new-in-docker-1-13/
