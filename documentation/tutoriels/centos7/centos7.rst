
.. index::
   pair: Tutoriel ; Centos7


.. _tuto_centos7:

===============================
Centos7
===============================

.. seealso::

   - http://www.codeghar.com/blog/install-latest-python-on-centos-7.html



Plan de travail
=================

- récupérer une image centos:7
- yum update
- yum install -y https://centos7.iuscommunity.org/ius-release.rpm
- yum install -y python36u python36u-libs python36u-devel python36u-pip
- yum install which
- yum install openldap-devel
- pip3.6 install pipenv


yum update
===========

::

    [root@20c8bd8c86f4 intranet]# yum update

::

	Loaded plugins: fastestmirror, ovl
	Loading mirror speeds from cached hostfile
	* base: ftp.pasteur.fr
	* epel: pkg.adfinis-sygroup.ch
	* extras: mirror.plusserver.com
	* ius: mirror.slu.cz
	* updates: ftp.ciril.fr
	Resolving Dependencies
	--> Running transaction check
	---> Package bind-license.noarch 32:9.9.4-51.el7_4.1 will be updated
	---> Package bind-license.noarch 32:9.9.4-51.el7_4.2 will be an update
	---> Package binutils.x86_64 0:2.25.1-32.base.el7_4.1 will be updated
	---> Package binutils.x86_64 0:2.25.1-32.base.el7_4.2 will be an update
	---> Package epel-release.noarch 0:7-9 will be updated
	---> Package epel-release.noarch 0:7-11 will be an update
	---> Package kmod.x86_64 0:20-15.el7_4.6 will be updated
	---> Package kmod.x86_64 0:20-15.el7_4.7 will be an update
	---> Package kmod-libs.x86_64 0:20-15.el7_4.6 will be updated
	---> Package kmod-libs.x86_64 0:20-15.el7_4.7 will be an update
	---> Package kpartx.x86_64 0:0.4.9-111.el7 will be updated
	---> Package kpartx.x86_64 0:0.4.9-111.el7_4.2 will be an update
	---> Package libdb.x86_64 0:5.3.21-20.el7 will be updated
	---> Package libdb.x86_64 0:5.3.21-21.el7_4 will be an update
	---> Package libdb-utils.x86_64 0:5.3.21-20.el7 will be updated
	---> Package libdb-utils.x86_64 0:5.3.21-21.el7_4 will be an update
	---> Package systemd.x86_64 0:219-42.el7_4.4 will be updated
	---> Package systemd.x86_64 0:219-42.el7_4.7 will be an update
	---> Package systemd-libs.x86_64 0:219-42.el7_4.4 will be updated
	---> Package systemd-libs.x86_64 0:219-42.el7_4.7 will be an update
	---> Package tzdata.noarch 0:2017c-1.el7 will be updated
	---> Package tzdata.noarch 0:2018c-1.el7 will be an update
	---> Package yum.noarch 0:3.4.3-154.el7.centos will be updated
	---> Package yum.noarch 0:3.4.3-154.el7.centos.1 will be an update
	--> Finished Dependency Resolution

	Dependencies Resolved

	===============================================================================================================================================================
	Package                               Arch                            Version                                          Repository                        Size
	===============================================================================================================================================================
	Updating:
	bind-license                          noarch                          32:9.9.4-51.el7_4.2                              updates                           84 k
	binutils                              x86_64                          2.25.1-32.base.el7_4.2                           updates                          5.4 M
	epel-release                          noarch                          7-11                                             epel                              15 k
	kmod                                  x86_64                          20-15.el7_4.7                                    updates                          121 k
	kmod-libs                             x86_64                          20-15.el7_4.7                                    updates                           50 k
	kpartx                                x86_64                          0.4.9-111.el7_4.2                                updates                           73 k
	libdb                                 x86_64                          5.3.21-21.el7_4                                  updates                          719 k
	libdb-utils                           x86_64                          5.3.21-21.el7_4                                  updates                          132 k
	systemd                               x86_64                          219-42.el7_4.7                                   updates                          5.2 M
	systemd-libs                          x86_64                          219-42.el7_4.7                                   updates                          376 k
	tzdata                                noarch                          2018c-1.el7                                      updates                          479 k
	yum                                   noarch                          3.4.3-154.el7.centos.1                           updates                          1.2 M

	Transaction Summary
	===============================================================================================================================================================
	Upgrade  12 Packages

	Total download size: 14 M
	Is this ok [y/d/N]: y
	Downloading packages:
	Delta RPMs disabled because /usr/bin/applydeltarpm not installed.
	(1/12): bind-license-9.9.4-51.el7_4.2.noarch.rpm                                                                                        |  84 kB  00:00:00
	(2/12): kmod-libs-20-15.el7_4.7.x86_64.rpm                                                                                              |  50 kB  00:00:00
	(3/12): kmod-20-15.el7_4.7.x86_64.rpm                                                                                                   | 121 kB  00:00:00
	warning: /var/cache/yum/x86_64/7/epel/packages/epel-release-7-11.noarch.rpm: Header V3 RSA/SHA256 Signature, key ID 352c64e5: NOKEY
	Public key for epel-release-7-11.noarch.rpm is not installed
	(4/12): epel-release-7-11.noarch.rpm                                                                                                    |  15 kB  00:00:00
	(5/12): libdb-utils-5.3.21-21.el7_4.x86_64.rpm                                                                                          | 132 kB  00:00:00
	(6/12): kpartx-0.4.9-111.el7_4.2.x86_64.rpm                                                                                             |  73 kB  00:00:00
	(7/12): libdb-5.3.21-21.el7_4.x86_64.rpm                                                                                                | 719 kB  00:00:01
	(8/12): tzdata-2018c-1.el7.noarch.rpm                                                                                                   | 479 kB  00:00:01
	(9/12): systemd-libs-219-42.el7_4.7.x86_64.rpm                                                                                          | 376 kB  00:00:02
	(10/12): yum-3.4.3-154.el7.centos.1.noarch.rpm                                                                                          | 1.2 MB  00:00:03
	(11/12): binutils-2.25.1-32.base.el7_4.2.x86_64.rpm                                                                                     | 5.4 MB  00:00:10
	(12/12): systemd-219-42.el7_4.7.x86_64.rpm                                                                                              | 5.2 MB  00:00:10
	---------------------------------------------------------------------------------------------------------------------------------------------------------------
	Total                                                                                                                          1.2 MB/s |  14 MB  00:00:11
	Retrieving key from file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7
	Importing GPG key 0x352C64E5:
	Userid     : "Fedora EPEL (7) <epel@fedoraproject.org>"
	Fingerprint: 91e9 7d7c 4a5e 96f1 7f3e 888f 6a2f aea2 352c 64e5
	Package    : epel-release-7-9.noarch (@extras)
	From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7
	Is this ok [y/N]: y
	Running transaction check
	Running transaction test
	Transaction test succeeded
	Running transaction
	Updating   : libdb-5.3.21-21.el7_4.x86_64                                                                                                               1/24
	Updating   : binutils-2.25.1-32.base.el7_4.2.x86_64                                                                                                     2/24
	Updating   : kmod-20-15.el7_4.7.x86_64                                                                                                                  3/24
	Updating   : systemd-libs-219-42.el7_4.7.x86_64                                                                                                         4/24
	Updating   : kmod-libs-20-15.el7_4.7.x86_64                                                                                                             5/24
	Updating   : systemd-219-42.el7_4.7.x86_64                                                                                                              6/24
	Updating   : libdb-utils-5.3.21-21.el7_4.x86_64                                                                                                         7/24
	Updating   : yum-3.4.3-154.el7.centos.1.noarch                                                                                                          8/24
	Updating   : 32:bind-license-9.9.4-51.el7_4.2.noarch                                                                                                    9/24
	Updating   : tzdata-2018c-1.el7.noarch                                                                                                                 10/24
	Updating   : kpartx-0.4.9-111.el7_4.2.x86_64                                                                                                           11/24
	Updating   : epel-release-7-11.noarch                                                                                                                  12/24
	Cleanup    : systemd-219-42.el7_4.4.x86_64                                                                                                             13/24
	Cleanup    : kmod-20-15.el7_4.6.x86_64                                                                                                                 14/24
	Cleanup    : libdb-utils-5.3.21-20.el7.x86_64                                                                                                          15/24
	Cleanup    : yum-3.4.3-154.el7.centos.noarch                                                                                                           16/24
	Cleanup    : 32:bind-license-9.9.4-51.el7_4.1.noarch                                                                                                   17/24
	Cleanup    : tzdata-2017c-1.el7.noarch                                                                                                                 18/24
	Cleanup    : epel-release-7-9.noarch                                                                                                                   19/24
	Cleanup    : libdb-5.3.21-20.el7.x86_64                                                                                                                20/24
	Cleanup    : binutils-2.25.1-32.base.el7_4.1.x86_64                                                                                                    21/24
	Cleanup    : kmod-libs-20-15.el7_4.6.x86_64                                                                                                            22/24
	Cleanup    : systemd-libs-219-42.el7_4.4.x86_64                                                                                                        23/24
	Cleanup    : kpartx-0.4.9-111.el7.x86_64                                                                                                               24/24
	Verifying  : kmod-20-15.el7_4.7.x86_64                                                                                                                  1/24
	Verifying  : kmod-libs-20-15.el7_4.7.x86_64                                                                                                             2/24
	Verifying  : libdb-utils-5.3.21-21.el7_4.x86_64                                                                                                         3/24
	Verifying  : systemd-219-42.el7_4.7.x86_64                                                                                                              4/24
	Verifying  : epel-release-7-11.noarch                                                                                                                   5/24
	Verifying  : kpartx-0.4.9-111.el7_4.2.x86_64                                                                                                            6/24
	Verifying  : tzdata-2018c-1.el7.noarch                                                                                                                  7/24
	Verifying  : 32:bind-license-9.9.4-51.el7_4.2.noarch                                                                                                    8/24
	Verifying  : systemd-libs-219-42.el7_4.7.x86_64                                                                                                         9/24
	Verifying  : binutils-2.25.1-32.base.el7_4.2.x86_64                                                                                                    10/24
	Verifying  : libdb-5.3.21-21.el7_4.x86_64                                                                                                              11/24
	Verifying  : yum-3.4.3-154.el7.centos.1.noarch                                                                                                         12/24
	Verifying  : epel-release-7-9.noarch                                                                                                                   13/24
	Verifying  : binutils-2.25.1-32.base.el7_4.1.x86_64                                                                                                    14/24
	Verifying  : 32:bind-license-9.9.4-51.el7_4.1.noarch                                                                                                   15/24
	Verifying  : systemd-libs-219-42.el7_4.4.x86_64                                                                                                        16/24
	Verifying  : kmod-20-15.el7_4.6.x86_64                                                                                                                 17/24
	Verifying  : systemd-219-42.el7_4.4.x86_64                                                                                                             18/24
	Verifying  : libdb-utils-5.3.21-20.el7.x86_64                                                                                                          19/24
	Verifying  : kmod-libs-20-15.el7_4.6.x86_64                                                                                                            20/24
	Verifying  : tzdata-2017c-1.el7.noarch                                                                                                                 21/24
	Verifying  : kpartx-0.4.9-111.el7.x86_64                                                                                                               22/24
	Verifying  : yum-3.4.3-154.el7.centos.noarch                                                                                                           23/24
	Verifying  : libdb-5.3.21-20.el7.x86_64                                                                                                                24/24

	Updated:
	bind-license.noarch 32:9.9.4-51.el7_4.2   binutils.x86_64 0:2.25.1-32.base.el7_4.2   epel-release.noarch 0:7-11       kmod.x86_64 0:20-15.el7_4.7
	kmod-libs.x86_64 0:20-15.el7_4.7          kpartx.x86_64 0:0.4.9-111.el7_4.2          libdb.x86_64 0:5.3.21-21.el7_4   libdb-utils.x86_64 0:5.3.21-21.el7_4
	systemd.x86_64 0:219-42.el7_4.7           systemd-libs.x86_64 0:219-42.el7_4.7       tzdata.noarch 0:2018c-1.el7      yum.noarch 0:3.4.3-154.el7.centos.1

	Complete!
	[root@20c8bd8c86f4 intranet]#



yum install -y https://centos7.iuscommunity.org/ius-release.rpm
================================================================

::

    [root@20c8bd8c86f4 /]# yum install -y https://centos7.iuscommunity.org/ius-release.rpm

::

	Loaded plugins: fastestmirror, ovl
	ius-release.rpm                                                                                                                         | 8.1 kB  00:00:00
	Examining /var/tmp/yum-root-KswZN7/ius-release.rpm: ius-release-1.0-15.ius.centos7.noarch
	Marking /var/tmp/yum-root-KswZN7/ius-release.rpm to be installed
	Resolving Dependencies
	--> Running transaction check
	---> Package ius-release.noarch 0:1.0-15.ius.centos7 will be installed
	--> Processing Dependency: epel-release = 7 for package: ius-release-1.0-15.ius.centos7.noarch
	base                                                                                                                                    | 3.6 kB  00:00:00
	extras                                                                                                                                  | 3.4 kB  00:00:00
	updates                                                                                                                                 | 3.4 kB  00:00:00
	(1/4): extras/7/x86_64/primary_db                                                                                                       | 166 kB  00:00:00
	(2/4): base/7/x86_64/group_gz                                                                                                           | 156 kB  00:00:01
	(3/4): updates/7/x86_64/primary_db                                                                                                      | 6.0 MB  00:00:04
	(4/4): base/7/x86_64/primary_db                                                                                                         | 5.7 MB  00:00:14
	Determining fastest mirrors
	* base: ftp.pasteur.fr
	* extras: mirror.plusserver.com
	* updates: ftp.ciril.fr
	--> Running transaction check
	---> Package epel-release.noarch 0:7-9 will be installed
	--> Finished Dependency Resolution

	Dependencies Resolved

	===============================================================================================================================================================
	Package                               Arch                            Version                                     Repository                             Size
	===============================================================================================================================================================
	Installing:
	ius-release                           noarch                          1.0-15.ius.centos7                          /ius-release                          8.5 k
	Installing for dependencies:
	epel-release                          noarch                          7-9                                         extras                                 14 k

	Transaction Summary
	===============================================================================================================================================================
	Install  1 Package (+1 Dependent package)

	Total size: 23 k
	Total download size: 14 k
	Installed size: 33 k
	Downloading packages:
	warning: /var/cache/yum/x86_64/7/extras/packages/epel-release-7-9.noarch.rpm: Header V3 RSA/SHA256 Signature, key ID f4a80eb5: NOKEYB/s |    0 B  --:--:-- ETA
	Public key for epel-release-7-9.noarch.rpm is not installed
	epel-release-7-9.noarch.rpm                                                                                                             |  14 kB  00:00:00
	Retrieving key from file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
	Importing GPG key 0xF4A80EB5:
	Userid     : "CentOS-7 Key (CentOS 7 Official Signing Key) <security@centos.org>"
	Fingerprint: 6341 ab27 53d7 8a78 a7c2 7bb1 24c6 a8a7 f4a8 0eb5
	Package    : centos-release-7-4.1708.el7.centos.x86_64 (@CentOS)
	From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
	Running transaction check
	Running transaction test
	Transaction test succeeded
	Running transaction
	Installing : epel-release-7-9.noarch                                                                                                                     1/2
	Installing : ius-release-1.0-15.ius.centos7.noarch                                                                                                       2/2
	Verifying  : ius-release-1.0-15.ius.centos7.noarch                                                                                                       1/2
	Verifying  : epel-release-7-9.noarch                                                                                                                     2/2

	Installed:
	ius-release.noarch 0:1.0-15.ius.centos7

	Dependency Installed:
	epel-release.noarch 0:7-9

	Complete!



yum install -y python36u python36u-libs python36u-devel python36u-pip
======================================================================

::

    [root@20c8bd8c86f4 /]# yum install -y python36u python36u-libs python36u-devel python36u-pip

::

	Loaded plugins: fastestmirror, ovl
	epel/x86_64/metalink                                                                                                                    |  26 kB  00:00:00
	epel                                                                                                                                    | 4.7 kB  00:00:00
	ius                                                                                                                                     | 2.3 kB  00:00:00
	(1/4): epel/x86_64/group_gz                                                                                                             | 266 kB  00:00:01
	(2/4): ius/x86_64/primary_db                                                                                                            | 212 kB  00:00:01
	(3/4): epel/x86_64/primary_db                                                                                                           | 6.2 MB  00:00:05
	(4/4): epel/x86_64/updateinfo                                                                                                           | 880 kB  00:00:06
	Loading mirror speeds from cached hostfile
	* base: ftp.pasteur.fr
	* epel: ftp-stud.hs-esslingen.de
	* extras: mirror.plusserver.com
	* ius: mirror.team-cymru.org
	* updates: ftp.ciril.fr
	Resolving Dependencies
	--> Running transaction check
	---> Package python36u.x86_64 0:3.6.4-1.ius.centos7 will be installed
	---> Package python36u-devel.x86_64 0:3.6.4-1.ius.centos7 will be installed
	---> Package python36u-libs.x86_64 0:3.6.4-1.ius.centos7 will be installed
	---> Package python36u-pip.noarch 0:9.0.1-1.ius.centos7 will be installed
	--> Processing Dependency: python36u-setuptools for package: python36u-pip-9.0.1-1.ius.centos7.noarch
	--> Running transaction check
	---> Package python36u-setuptools.noarch 0:36.6.0-1.ius.centos7 will be installed
	--> Finished Dependency Resolution

	Dependencies Resolved

	===============================================================================================================================================================
	Package                                      Arch                           Version                                         Repository                   Size
	===============================================================================================================================================================
	Installing:
	python36u                                    x86_64                         3.6.4-1.ius.centos7                             ius                          56 k
	python36u-devel                              x86_64                         3.6.4-1.ius.centos7                             ius                         839 k
	python36u-libs                               x86_64                         3.6.4-1.ius.centos7                             ius                         8.7 M
	python36u-pip                                noarch                         9.0.1-1.ius.centos7                             ius                         1.8 M
	Installing for dependencies:
	python36u-setuptools                         noarch                         36.6.0-1.ius.centos7                            ius                         587 k

	Transaction Summary
	===============================================================================================================================================================
	Install  4 Packages (+1 Dependent package)

	Total download size: 12 M
	Installed size: 53 M
	Downloading packages:
	warning: /var/cache/yum/x86_64/7/ius/packages/python36u-3.6.4-1.ius.centos7.x86_64.rpm: Header V4 DSA/SHA1 Signature, key ID 9cd4953f: NOKEY2 kB  --:--:-- ETA
	Public key for python36u-3.6.4-1.ius.centos7.x86_64.rpm is not installed
	(1/5): python36u-3.6.4-1.ius.centos7.x86_64.rpm                                                                                         |  56 kB  00:00:00
	(2/5): python36u-setuptools-36.6.0-1.ius.centos7.noarch.rpm                                                                             | 587 kB  00:00:03
	(3/5): python36u-pip-9.0.1-1.ius.centos7.noarch.rpm                                                                                     | 1.8 MB  00:00:03
	(4/5): python36u-devel-3.6.4-1.ius.centos7.x86_64.rpm                                                                                   | 839 kB  00:00:06
	(5/5): python36u-libs-3.6.4-1.ius.centos7.x86_64.rpm                                                                                    | 8.7 MB  00:00:28
	---------------------------------------------------------------------------------------------------------------------------------------------------------------
	Total                                                                                                                          432 kB/s |  12 MB  00:00:28
	Retrieving key from file:///etc/pki/rpm-gpg/IUS-COMMUNITY-GPG-KEY
	Importing GPG key 0x9CD4953F:
	Userid     : "IUS Community Project <coredev@iuscommunity.org>"
	Fingerprint: 8b84 6e3a b3fe 6462 74e8 670f da22 1cdf 9cd4 953f
	Package    : ius-release-1.0-15.ius.centos7.noarch (installed)
	From       : /etc/pki/rpm-gpg/IUS-COMMUNITY-GPG-KEY
	Running transaction check
	Running transaction test
	Transaction test succeeded
	Running transaction
	Installing : python36u-libs-3.6.4-1.ius.centos7.x86_64                                                                                                   1/5
	Installing : python36u-3.6.4-1.ius.centos7.x86_64                                                                                                        2/5
	Installing : python36u-setuptools-36.6.0-1.ius.centos7.noarch                                                                                            3/5
	Installing : python36u-pip-9.0.1-1.ius.centos7.noarch                                                                                                    4/5
	Installing : python36u-devel-3.6.4-1.ius.centos7.x86_64                                                                                                  5/5
	Verifying  : python36u-setuptools-36.6.0-1.ius.centos7.noarch                                                                                            1/5
	Verifying  : python36u-pip-9.0.1-1.ius.centos7.noarch                                                                                                    2/5
	Verifying  : python36u-3.6.4-1.ius.centos7.x86_64                                                                                                        3/5
	Verifying  : python36u-libs-3.6.4-1.ius.centos7.x86_64                                                                                                   4/5
	Verifying  : python36u-devel-3.6.4-1.ius.centos7.x86_64                                                                                                  5/5

	Installed:
	python36u.x86_64 0:3.6.4-1.ius.centos7             python36u-devel.x86_64 0:3.6.4-1.ius.centos7         python36u-libs.x86_64 0:3.6.4-1.ius.centos7
	python36u-pip.noarch 0:9.0.1-1.ius.centos7

	Dependency Installed:
	python36u-setuptools.noarch 0:36.6.0-1.ius.centos7

	Complete!
	[root@20c8bd8c86f4 /]#



python3.6
===========

::

	[root@20c8bd8c86f4 /]# python3.6
	Python 3.6.4 (default, Dec 19 2017, 14:48:12)
	[GCC 4.8.5 20150623 (Red Hat 4.8.5-16)] on linux
	Type "help", "copyright", "credits" or "license" for more information.
	>>>


yum install which
===================

::

    [root@20c8bd8c86f4 /]# yum install which

::

	Loaded plugins: fastestmirror, ovl
	Loading mirror speeds from cached hostfile
	 * base: ftp.pasteur.fr
	 * epel: repo.boun.edu.tr
	 * extras: mirror.plusserver.com
	 * ius: mirror.its.dal.ca
	 * updates: ftp.ciril.fr
	Resolving Dependencies
	--> Running transaction check
	---> Package which.x86_64 0:2.20-7.el7 will be installed
	--> Finished Dependency Resolution

	Dependencies Resolved

	===============================================================================================================================================================
	 Package                             Arch                                 Version                                     Repository                          Size
	===============================================================================================================================================================
	Installing:
	 which                               x86_64                               2.20-7.el7                                  base                                41 k

	Transaction Summary
	===============================================================================================================================================================
	Install  1 Package

	Total download size: 41 k
	Installed size: 75 k
	Is this ok [y/d/N]: y
	Downloading packages:
	which-2.20-7.el7.x86_64.rpm                                                                                                             |  41 kB  00:00:00
	Running transaction check
	Running transaction test
	Transaction test succeeded
	Running transaction
	  Installing : which-2.20-7.el7.x86_64                                                                                                                     1/1
	install-info: No such file or directory for /usr/share/info/which.info.gz
	  Verifying  : which-2.20-7.el7.x86_64                                                                                                                     1/1

	Installed:
	  which.x86_64 0:2.20-7.el7

	Complete!
	[root@20c8bd8c86f4 /]# which python3.6
	/usr/bin/python3.6

	[root@20c8bd8c86f4 /]# which python3.6
	/usr/bin/python3.6



which pip3.6
=============

::

    [root@20c8bd8c86f4 /]# which pip3.6

::

    /usr/bin/pip3.6

::

    [root@20c8bd8c86f4 /]# pip3.6 install pipenv

::

	Collecting pipenv
	Downloading pipenv-9.0.3.tar.gz (3.9MB)
	100% |################################| 3.9MB 291kB/s
	Collecting virtualenv (from pipenv)
	Downloading virtualenv-15.1.0-py2.py3-none-any.whl (1.8MB)
	100% |################################| 1.8MB 610kB/s
	Collecting pew>=0.1.26 (from pipenv)
	Downloading pew-1.1.2-py2.py3-none-any.whl
	Requirement already satisfied: pip>=9.0.1 in /usr/lib/python3.6/site-packages (from pipenv)
	Collecting requests>2.18.0 (from pipenv)
	Downloading requests-2.18.4-py2.py3-none-any.whl (88kB)
	100% |################################| 92kB 1.1MB/s
	Collecting flake8>=3.0.0 (from pipenv)
	Downloading flake8-3.5.0-py2.py3-none-any.whl (69kB)
	100% |################################| 71kB 2.8MB/s
	Collecting urllib3>=1.21.1 (from pipenv)
	Downloading urllib3-1.22-py2.py3-none-any.whl (132kB)
	100% |################################| 133kB 2.0MB/s
	Requirement already satisfied: setuptools>=17.1 in /usr/lib/python3.6/site-packages (from pew>=0.1.26->pipenv)
	Collecting virtualenv-clone>=0.2.5 (from pew>=0.1.26->pipenv)
	Downloading virtualenv-clone-0.2.6.tar.gz
	Collecting certifi>=2017.4.17 (from requests>2.18.0->pipenv)
	Downloading certifi-2018.1.18-py2.py3-none-any.whl (151kB)
	100% |################################| 153kB 1.0MB/s
	Collecting chardet<3.1.0,>=3.0.2 (from requests>2.18.0->pipenv)
	Downloading chardet-3.0.4-py2.py3-none-any.whl (133kB)
	100% |################################| 143kB 2.4MB/s
	Collecting idna<2.7,>=2.5 (from requests>2.18.0->pipenv)
	Downloading idna-2.6-py2.py3-none-any.whl (56kB)
	100% |################################| 61kB 920kB/s
	Collecting mccabe<0.7.0,>=0.6.0 (from flake8>=3.0.0->pipenv)
	Downloading mccabe-0.6.1-py2.py3-none-any.whl
	Collecting pycodestyle<2.4.0,>=2.0.0 (from flake8>=3.0.0->pipenv)
	Downloading pycodestyle-2.3.1-py2.py3-none-any.whl (45kB)
	100% |################################| 51kB 2.2MB/s
	Collecting pyflakes<1.7.0,>=1.5.0 (from flake8>=3.0.0->pipenv)
	Downloading pyflakes-1.6.0-py2.py3-none-any.whl (227kB)
	100% |################################| 235kB 2.3MB/s
	Installing collected packages: virtualenv, virtualenv-clone, pew, certifi, urllib3, chardet, idna, requests, mccabe, pycodestyle, pyflakes, flake8, pipenv
	Running setup.py install for virtualenv-clone ... done
	Running setup.py install for pipenv ... done
	Successfully installed certifi-2018.1.18 chardet-3.0.4 flake8-3.5.0 idna-2.6 mccabe-0.6.1 pew-1.1.2 pipenv-9.0.3 pycodestyle-2.3.1 pyflakes-1.6.0 requests-2.18.4 urllib3-1.22 virtualenv-15.1.0 virtualenv-clone-0.2.6



::

	(activate) [root@20c8bd8c86f4 intranet]# pip install django
	Collecting django
	  Downloading Django-2.0.2-py3-none-any.whl (7.1MB)
		100% |################################| 7.1MB 205kB/s
	Collecting pytz (from django)
	  Downloading pytz-2017.3-py2.py3-none-any.whl (511kB)
		100% |################################| 512kB 1.5MB/s
	Installing collected packages: pytz, django
	Successfully installed django-2.0.2 pytz-2017.3


docker build -t id3centos7:1 .
=================================

::

    PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\centos7> docker build -t id3centos7:1 .

::

	Sending build context to Docker daemon  37.38kB
	Step 1/5 : FROM centos:7
	---> ff426288ea90
	Step 2/5 : RUN yum update -y
	---> Running in bd9bc627aeeb
	Loaded plugins: fastestmirror, ovl
	Determining fastest mirrors
	* base: centos.quelquesmots.fr
	* extras: fr.mirror.babylon.network
	* updates: fr.mirror.babylon.network
	Resolving Dependencies
	--> Running transaction check
	---> Package bind-license.noarch 32:9.9.4-51.el7_4.1 will be updated
	---> Package bind-license.noarch 32:9.9.4-51.el7_4.2 will be an update
	---> Package binutils.x86_64 0:2.25.1-32.base.el7_4.1 will be updated
	---> Package binutils.x86_64 0:2.25.1-32.base.el7_4.2 will be an update
	---> Package kmod.x86_64 0:20-15.el7_4.6 will be updated
	---> Package kmod.x86_64 0:20-15.el7_4.7 will be an update
	---> Package kmod-libs.x86_64 0:20-15.el7_4.6 will be updated
	---> Package kmod-libs.x86_64 0:20-15.el7_4.7 will be an update
	---> Package kpartx.x86_64 0:0.4.9-111.el7 will be updated
	---> Package kpartx.x86_64 0:0.4.9-111.el7_4.2 will be an update
	---> Package libdb.x86_64 0:5.3.21-20.el7 will be updated
	---> Package libdb.x86_64 0:5.3.21-21.el7_4 will be an update
	---> Package libdb-utils.x86_64 0:5.3.21-20.el7 will be updated
	---> Package libdb-utils.x86_64 0:5.3.21-21.el7_4 will be an update
	---> Package systemd.x86_64 0:219-42.el7_4.4 will be updated
	---> Package systemd.x86_64 0:219-42.el7_4.7 will be an update
	---> Package systemd-libs.x86_64 0:219-42.el7_4.4 will be updated
	---> Package systemd-libs.x86_64 0:219-42.el7_4.7 will be an update
	---> Package tzdata.noarch 0:2017c-1.el7 will be updated
	---> Package tzdata.noarch 0:2018c-1.el7 will be an update
	---> Package yum.noarch 0:3.4.3-154.el7.centos will be updated
	---> Package yum.noarch 0:3.4.3-154.el7.centos.1 will be an update
	--> Finished Dependency Resolution

	Dependencies Resolved

	================================================================================
	Package           Arch        Version                       Repository    Size
	================================================================================
	Updating:
	bind-license      noarch      32:9.9.4-51.el7_4.2           updates       84 k
	binutils          x86_64      2.25.1-32.base.el7_4.2        updates      5.4 M
	kmod              x86_64      20-15.el7_4.7                 updates      121 k
	kmod-libs         x86_64      20-15.el7_4.7                 updates       50 k
	kpartx            x86_64      0.4.9-111.el7_4.2             updates       73 k
	libdb             x86_64      5.3.21-21.el7_4               updates      719 k
	libdb-utils       x86_64      5.3.21-21.el7_4               updates      132 k
	systemd           x86_64      219-42.el7_4.7                updates      5.2 M
	systemd-libs      x86_64      219-42.el7_4.7                updates      376 k
	tzdata            noarch      2018c-1.el7                   updates      479 k
	yum               noarch      3.4.3-154.el7.centos.1        updates      1.2 M

	Transaction Summary
	================================================================================
	Upgrade  11 Packages

	Total download size: 14 M
	Downloading packages:
	Delta RPMs disabled because /usr/bin/applydeltarpm not installed.
	warning: /var/cache/yum/x86_64/7/updates/packages/kmod-libs-20-15.el7_4.7.x86_64.rpm: Header V3 RSA/SHA256 Signature, key ID f4a80eb5: NOKEY
	Public key for kmod-libs-20-15.el7_4.7.x86_64.rpm is not installed
	--------------------------------------------------------------------------------
	Total                                              1.6 MB/s |  14 MB  00:08
	Retrieving key from file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
	Importing GPG key 0xF4A80EB5:
	Userid     : "CentOS-7 Key (CentOS 7 Official Signing Key) <security@centos.org>"
	Fingerprint: 6341 ab27 53d7 8a78 a7c2 7bb1 24c6 a8a7 f4a8 0eb5
	Package    : centos-release-7-4.1708.el7.centos.x86_64 (@CentOS)
	From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
	Running transaction check
	Running transaction test
	Transaction test succeeded
	Running transaction
	Updating   : libdb-5.3.21-21.el7_4.x86_64                                1/22
	Updating   : binutils-2.25.1-32.base.el7_4.2.x86_64                      2/22
	Updating   : kmod-20-15.el7_4.7.x86_64                                   3/22
	Updating   : systemd-libs-219-42.el7_4.7.x86_64                          4/22
	Updating   : kmod-libs-20-15.el7_4.7.x86_64                              5/22
	Updating   : systemd-219-42.el7_4.7.x86_64                               6/22
	Updating   : libdb-utils-5.3.21-21.el7_4.x86_64                          7/22
	Updating   : yum-3.4.3-154.el7.centos.1.noarch                           8/22
	Updating   : 32:bind-license-9.9.4-51.el7_4.2.noarch                     9/22
	Updating   : tzdata-2018c-1.el7.noarch                                  10/22
	Updating   : kpartx-0.4.9-111.el7_4.2.x86_64                            11/22
	Cleanup    : systemd-219-42.el7_4.4.x86_64                              12/22
	Cleanup    : kmod-20-15.el7_4.6.x86_64                                  13/22
	Cleanup    : libdb-utils-5.3.21-20.el7.x86_64                           14/22
	Cleanup    : yum-3.4.3-154.el7.centos.noarch                            15/22
	Cleanup    : 32:bind-license-9.9.4-51.el7_4.1.noarch                    16/22
	Cleanup    : tzdata-2017c-1.el7.noarch                                  17/22
	Cleanup    : libdb-5.3.21-20.el7.x86_64                                 18/22
	Cleanup    : binutils-2.25.1-32.base.el7_4.1.x86_64                     19/22
	Cleanup    : kmod-libs-20-15.el7_4.6.x86_64                             20/22
	Cleanup    : systemd-libs-219-42.el7_4.4.x86_64                         21/22
	Cleanup    : kpartx-0.4.9-111.el7.x86_64                                22/22
	Verifying  : kmod-20-15.el7_4.7.x86_64                                   1/22
	Verifying  : kmod-libs-20-15.el7_4.7.x86_64                              2/22
	Verifying  : libdb-utils-5.3.21-21.el7_4.x86_64                          3/22
	Verifying  : systemd-219-42.el7_4.7.x86_64                               4/22
	Verifying  : kpartx-0.4.9-111.el7_4.2.x86_64                             5/22
	Verifying  : tzdata-2018c-1.el7.noarch                                   6/22
	Verifying  : 32:bind-license-9.9.4-51.el7_4.2.noarch                     7/22
	Verifying  : systemd-libs-219-42.el7_4.7.x86_64                          8/22
	Verifying  : binutils-2.25.1-32.base.el7_4.2.x86_64                      9/22
	Verifying  : libdb-5.3.21-21.el7_4.x86_64                               10/22
	Verifying  : yum-3.4.3-154.el7.centos.1.noarch                          11/22
	Verifying  : binutils-2.25.1-32.base.el7_4.1.x86_64                     12/22
	Verifying  : 32:bind-license-9.9.4-51.el7_4.1.noarch                    13/22
	Verifying  : systemd-libs-219-42.el7_4.4.x86_64                         14/22
	Verifying  : kmod-20-15.el7_4.6.x86_64                                  15/22
	Verifying  : systemd-219-42.el7_4.4.x86_64                              16/22
	Verifying  : libdb-utils-5.3.21-20.el7.x86_64                           17/22
	Verifying  : kmod-libs-20-15.el7_4.6.x86_64                             18/22
	Verifying  : tzdata-2017c-1.el7.noarch                                  19/22
	Verifying  : kpartx-0.4.9-111.el7.x86_64                                20/22
	Verifying  : yum-3.4.3-154.el7.centos.noarch                            21/22
	Verifying  : libdb-5.3.21-20.el7.x86_64                                 22/22

	Updated:
	bind-license.noarch 32:9.9.4-51.el7_4.2
	binutils.x86_64 0:2.25.1-32.base.el7_4.2
	kmod.x86_64 0:20-15.el7_4.7
	kmod-libs.x86_64 0:20-15.el7_4.7
	kpartx.x86_64 0:0.4.9-111.el7_4.2
	libdb.x86_64 0:5.3.21-21.el7_4
	libdb-utils.x86_64 0:5.3.21-21.el7_4
	systemd.x86_64 0:219-42.el7_4.7
	systemd-libs.x86_64 0:219-42.el7_4.7
	tzdata.noarch 0:2018c-1.el7
	yum.noarch 0:3.4.3-154.el7.centos.1

	Complete!
	Removing intermediate container bd9bc627aeeb
	---> 90814f4b95d5
	Step 3/5 : RUN yum install -y https://centos7.iuscommunity.org/ius-release.rpm
	---> Running in cea6a40470fa
	Loaded plugins: fastestmirror, ovl
	Examining /var/tmp/yum-root-Z3I8ac/ius-release.rpm: ius-release-1.0-15.ius.centos7.noarch
	Marking /var/tmp/yum-root-Z3I8ac/ius-release.rpm to be installed
	Resolving Dependencies
	--> Running transaction check
	---> Package ius-release.noarch 0:1.0-15.ius.centos7 will be installed
	--> Processing Dependency: epel-release = 7 for package: ius-release-1.0-15.ius.centos7.noarch
	Loading mirror speeds from cached hostfile
	* base: centos.quelquesmots.fr
	* extras: fr.mirror.babylon.network
	* updates: fr.mirror.babylon.network
	--> Running transaction check
	---> Package epel-release.noarch 0:7-9 will be installed
	--> Finished Dependency Resolution

	Dependencies Resolved

	================================================================================
	Package           Arch        Version                  Repository         Size
	================================================================================
	Installing:
	ius-release       noarch      1.0-15.ius.centos7       /ius-release      8.5 k
	Installing for dependencies:
	epel-release      noarch      7-9                      extras             14 k

	Transaction Summary
	================================================================================
	Install  1 Package (+1 Dependent package)

	Total size: 23 k
	Total download size: 14 k
	Installed size: 33 k
	Downloading packages:
	Running transaction check
	Running transaction test
	Transaction test succeeded
	Running transaction
	Installing : epel-release-7-9.noarch                                      1/2
	Installing : ius-release-1.0-15.ius.centos7.noarch                        2/2
	Verifying  : ius-release-1.0-15.ius.centos7.noarch                        1/2
	Verifying  : epel-release-7-9.noarch                                      2/2

	Installed:
	ius-release.noarch 0:1.0-15.ius.centos7

	Dependency Installed:
	epel-release.noarch 0:7-9

	Complete!
	Removing intermediate container cea6a40470fa
	---> b9963da64678
	Step 4/5 : RUN yum install -y python36u python36u-libs python36u-devel python36u-pip
	---> Running in f9691783f72c
	Loaded plugins: fastestmirror, ovl
	Loading mirror speeds from cached hostfile
	* base: centos.quelquesmots.fr
	* epel: fr.mirror.babylon.network
	* extras: fr.mirror.babylon.network
	* ius: mirrors.tongji.edu.cn
	* updates: fr.mirror.babylon.network
	Resolving Dependencies
	--> Running transaction check
	---> Package python36u.x86_64 0:3.6.4-1.ius.centos7 will be installed
	---> Package python36u-devel.x86_64 0:3.6.4-1.ius.centos7 will be installed
	---> Package python36u-libs.x86_64 0:3.6.4-1.ius.centos7 will be installed
	---> Package python36u-pip.noarch 0:9.0.1-1.ius.centos7 will be installed
	--> Processing Dependency: python36u-setuptools for package: python36u-pip-9.0.1-1.ius.centos7.noarch
	--> Running transaction check
	---> Package python36u-setuptools.noarch 0:36.6.0-1.ius.centos7 will be installed
	--> Finished Dependency Resolution

	Dependencies Resolved

	================================================================================
	Package                   Arch        Version                   Repository
																		   Size
	================================================================================
	Installing:
	python36u                 x86_64      3.6.4-1.ius.centos7       ius       56 k
	python36u-devel           x86_64      3.6.4-1.ius.centos7       ius      839 k
	python36u-libs            x86_64      3.6.4-1.ius.centos7       ius      8.7 M
	python36u-pip             noarch      9.0.1-1.ius.centos7       ius      1.8 M
	Installing for dependencies:
	python36u-setuptools      noarch      36.6.0-1.ius.centos7      ius      587 k

	Transaction Summary
	================================================================================
	Install  4 Packages (+1 Dependent package)

	Total download size: 12 M
	Installed size: 53 M
	Downloading packages:
	warning: /var/cache/yum/x86_64/7/ius/packages/python36u-devel-3.6.4-1.ius.centos7.x86_64.rpm: Header V4 DSA/SHA1 Signature, key ID 9cd4953f: NOKEY
	Public key for python36u-devel-3.6.4-1.ius.centos7.x86_64.rpm is not installed
	--------------------------------------------------------------------------------
	Total                                              1.0 MB/s |  12 MB  00:12
	Retrieving key from file:///etc/pki/rpm-gpg/IUS-COMMUNITY-GPG-KEY
	Importing GPG key 0x9CD4953F:
	Userid     : "IUS Community Project <coredev@iuscommunity.org>"
	Fingerprint: 8b84 6e3a b3fe 6462 74e8 670f da22 1cdf 9cd4 953f
	Package    : ius-release-1.0-15.ius.centos7.noarch (installed)
	From       : /etc/pki/rpm-gpg/IUS-COMMUNITY-GPG-KEY
	Running transaction check
	Running transaction test
	Transaction test succeeded
	Running transaction
	Installing : python36u-libs-3.6.4-1.ius.centos7.x86_64                    1/5
	Installing : python36u-3.6.4-1.ius.centos7.x86_64                         2/5
	Installing : python36u-setuptools-36.6.0-1.ius.centos7.noarch             3/5
	Installing : python36u-pip-9.0.1-1.ius.centos7.noarch                     4/5
	Installing : python36u-devel-3.6.4-1.ius.centos7.x86_64                   5/5
	Verifying  : python36u-setuptools-36.6.0-1.ius.centos7.noarch             1/5
	Verifying  : python36u-pip-9.0.1-1.ius.centos7.noarch                     2/5
	Verifying  : python36u-3.6.4-1.ius.centos7.x86_64                         3/5
	Verifying  : python36u-libs-3.6.4-1.ius.centos7.x86_64                    4/5
	Verifying  : python36u-devel-3.6.4-1.ius.centos7.x86_64                   5/5

	Installed:
	python36u.x86_64 0:3.6.4-1.ius.centos7
	python36u-devel.x86_64 0:3.6.4-1.ius.centos7
	python36u-libs.x86_64 0:3.6.4-1.ius.centos7
	python36u-pip.noarch 0:9.0.1-1.ius.centos7

	Dependency Installed:
	python36u-setuptools.noarch 0:36.6.0-1.ius.centos7

	Complete!
	Removing intermediate container f9691783f72c
	---> 2edcf9418ddb
	Step 5/5 : RUN yum install -y which
	---> Running in b7bf8af2a677
	Loaded plugins: fastestmirror, ovl
	Loading mirror speeds from cached hostfile
	* base: centos.quelquesmots.fr
	* epel: mirror.airenetworks.es
	* extras: fr.mirror.babylon.network
	* ius: mirrors.ircam.fr
	* updates: fr.mirror.babylon.network
	Resolving Dependencies
	--> Running transaction check
	---> Package which.x86_64 0:2.20-7.el7 will be installed
	--> Finished Dependency Resolution

	Dependencies Resolved

	================================================================================
	Package          Arch              Version               Repository       Size
	================================================================================
	Installing:
	which            x86_64            2.20-7.el7            base             41 k

	Transaction Summary
	================================================================================
	Install  1 Package

	Total download size: 41 k
	Installed size: 75 k
	Downloading packages:
	Running transaction check
	Running transaction test
	Transaction test succeeded
	Running transaction
	Installing : which-2.20-7.el7.x86_64                                      1/1
	install-info: No such file or directory for /usr/share/info/which.info.gz
	Verifying  : which-2.20-7.el7.x86_64                                      1/1

	Installed:
	which.x86_64 0:2.20-7.el7

	Complete!
	Removing intermediate container b7bf8af2a677
	---> c0efabb4e2cb
	Successfully built c0efabb4e2cb
	Successfully tagged id3centos7:1
	SECURITY WARNING: You are building a Docker image from Windows against a non-Windows Docker host. All files and directories added to build context will have '-rwxr-xr-x' permissions. It is recommended to double check and reset permissions for sensitive files and directories.
	PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\centos7> docker images
	REPOSITORY                 TAG                 IMAGE ID            CREATED             SIZE
	id3centos7                 1                   c0efabb4e2cb        54 seconds ago      770MB
	ch4messageboardapp_web     latest              a08febb741e4        17 hours ago        782MB
	postgres                   10.1                b820823c41bd        17 hours ago        290MB
	<none>                     <none>              62b12eb064b3        17 hours ago        729MB
	<none>                     <none>              46dc0ae69726        17 hours ago        729MB
	<none>                     <none>              b940cde74b73        17 hours ago        920MB
	<none>                     <none>              ad18d8d88ab0        18 hours ago        920MB
	<none>                     <none>              71e39ba2a7bb        18 hours ago        729MB
	<none>                     <none>              9fda17d01d46        18 hours ago        729MB
	<none>                     <none>              326079a0d350        18 hours ago        772MB
	<none>                     <none>              a617107b453b        18 hours ago        772MB
	<none>                     <none>              8fdb1af40b0f        19 hours ago        729MB
	centos                     7                   ff426288ea90        3 weeks ago         207MB
	nginx                      latest              3f8a4339aadd        5 weeks ago         108MB
	python                     3.6                 c1e459c00dc3        6 weeks ago         692MB
	postgres                   <none>              ec61d13c8566        7 weeks ago         287MB
	docker4w/nsenter-dockerd   latest              cae870735e91        3 months ago        187kB
	PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\centos7> doc

docker images
===============

::

    PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\centos7> docker images

::

	REPOSITORY                 TAG                 IMAGE ID            CREATED             SIZE
	id3centos7                 1                   c0efabb4e2cb        54 seconds ago      770MB
	ch4messageboardapp_web     latest              a08febb741e4        17 hours ago        782MB
	postgres                   10.1                b820823c41bd        17 hours ago        290MB
	<none>                     <none>              62b12eb064b3        17 hours ago        729MB
	<none>                     <none>              46dc0ae69726        17 hours ago        729MB
	<none>                     <none>              b940cde74b73        17 hours ago        920MB
	<none>                     <none>              ad18d8d88ab0        18 hours ago        920MB
	<none>                     <none>              71e39ba2a7bb        18 hours ago        729MB
	<none>                     <none>              9fda17d01d46        18 hours ago        729MB
	<none>                     <none>              326079a0d350        18 hours ago        772MB
	<none>                     <none>              a617107b453b        18 hours ago        772MB
	<none>                     <none>              8fdb1af40b0f        19 hours ago        729MB
	centos                     7                   ff426288ea90        3 weeks ago         207MB
	nginx                      latest              3f8a4339aadd        5 weeks ago         108MB
	python                     3.6                 c1e459c00dc3        6 weeks ago         692MB
	postgres                   <none>              ec61d13c8566        7 weeks ago         287MB
	docker4w/nsenter-dockerd   latest              cae870735e91        3 months ago        187kB


docker run --name test -it id3centos7:1
===========================================



Probleme avec regex
======================

regex = "*"

::

	----------------------------------------
	Failed building wheel for regex
	Running setup.py clean for regex
	Failed to build regex
	Installing collected packages: regex
	Running setup.py install for regex ... error
	Complete output from command /opt/intranet/intranet/bin/python3.6 -u -c "import setuptools, tokenize;__file__='/tmp/pip-build-rrdh2091/regex/setup.py';f=getattr(tokenize, 'open', open)(__file__);code=f.read().replace('\r\n', '\n');f.close();exec(compile(code, __file__, 'exec'))" install --record /tmp/pip-fjizm5wj-record/install-record.txt --single-version-externally-managed --compile --install-headers /opt/intranet/intranet/include/site/python3.6/regex:
	/opt/intranet/intranet/lib/python3.6/site-packages/setuptools/dist.py:355: UserWarning: Normalizing '2018.01.10' to '2018.1.10'
	normalized_version,
	running install
	running build
	running build_py
	creating build
	creating build/lib.linux-x86_64-3.6
	copying regex_3/regex.py -> build/lib.linux-x86_64-3.6
	copying regex_3/_regex_core.py -> build/lib.linux-x86_64-3.6
	copying regex_3/test_regex.py -> build/lib.linux-x86_64-3.6
	running build_ext
	building '_regex' extension
	creating build/temp.linux-x86_64-3.6
	creating build/temp.linux-x86_64-3.6/regex_3
	gcc -pthread -Wno-unused-result -Wsign-compare -DDYNAMIC_ANNOTATIONS_ENABLED=1 -DNDEBUG -O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -m64 -mtune=generic -D_GNU_SOURCE -fPIC -fwrapv -fPIC -I/usr/include/python3.6m -c regex_3/_regex.c -o build/temp.linux-x86_64-3.6/regex_3/_regex.o
	unable to execute 'gcc': No such file or directory
	error: command 'gcc' failed with exit status 1

		----------------------------------------
	Command "/opt/intranet/intranet/bin/python3.6 -u -c "import setuptools, tokenize;__file__='/tmp/pip-build-rrdh2091/regex/setup.py';f=getattr(tokenize, 'open', open)(__file__);code=f.read().replace('\r\n', '\n');f.close();exec(compile(code, __file__, 'exec'))" install --record /tmp/pip-fjizm5wj-record/install-record.txt --single-version-externally-managed --compile --install-headers /opt/intranet/intranet/include/site/python3.6/regex" failed with error code 1 in /tmp/pip-build-rrdh2091/regex/
	(intranet) [root@35d914e8c996 intranet]# yum install gcc gcc-devel


yum install gcc
==================

::

    (intranet) [root@35d914e8c996 intranet]# yum install gcc gcc-devel


::

	Loaded plugins: fastestmirror, ovl
	Loading mirror speeds from cached hostfile
	* base: centos.quelquesmots.fr
	* epel: mirror.vutbr.cz
	* extras: fr.mirror.babylon.network
	* ius: mirror.team-cymru.org
	* updates: fr.mirror.babylon.network
	No package gcc-devel available.
	Resolving Dependencies
	--> Running transaction check
	---> Package gcc.x86_64 0:4.8.5-16.el7_4.1 will be installed
	--> Processing Dependency: libgomp = 4.8.5-16.el7_4.1 for package: gcc-4.8.5-16.el7_4.1.x86_64
	--> Processing Dependency: cpp = 4.8.5-16.el7_4.1 for package: gcc-4.8.5-16.el7_4.1.x86_64
	--> Processing Dependency: glibc-devel >= 2.2.90-12 for package: gcc-4.8.5-16.el7_4.1.x86_64
	--> Processing Dependency: libmpfr.so.4()(64bit) for package: gcc-4.8.5-16.el7_4.1.x86_64
	--> Processing Dependency: libmpc.so.3()(64bit) for package: gcc-4.8.5-16.el7_4.1.x86_64
	--> Processing Dependency: libgomp.so.1()(64bit) for package: gcc-4.8.5-16.el7_4.1.x86_64
	--> Running transaction check
	---> Package cpp.x86_64 0:4.8.5-16.el7_4.1 will be installed
	---> Package glibc-devel.x86_64 0:2.17-196.el7_4.2 will be installed
	--> Processing Dependency: glibc-headers = 2.17-196.el7_4.2 for package: glibc-devel-2.17-196.el7_4.2.x86_64
	--> Processing Dependency: glibc-headers for package: glibc-devel-2.17-196.el7_4.2.x86_64
	---> Package libgomp.x86_64 0:4.8.5-16.el7_4.1 will be installed
	---> Package libmpc.x86_64 0:1.0.1-3.el7 will be installed
	---> Package mpfr.x86_64 0:3.1.1-4.el7 will be installed
	--> Running transaction check
	---> Package glibc-headers.x86_64 0:2.17-196.el7_4.2 will be installed
	--> Processing Dependency: kernel-headers >= 2.2.1 for package: glibc-headers-2.17-196.el7_4.2.x86_64
	--> Processing Dependency: kernel-headers for package: glibc-headers-2.17-196.el7_4.2.x86_64
	--> Running transaction check
	---> Package kernel-headers.x86_64 0:3.10.0-693.17.1.el7 will be installed
	--> Finished Dependency Resolution

	Dependencies Resolved

	===========================================================================================================================================================================================
	Package                                        Arch                                   Version                                               Repository                               Size
	===========================================================================================================================================================================================
	Installing:
	gcc                                            x86_64                                 4.8.5-16.el7_4.1                                      updates                                  16 M
	Installing for dependencies:
	cpp                                            x86_64                                 4.8.5-16.el7_4.1                                      updates                                 5.9 M
	glibc-devel                                    x86_64                                 2.17-196.el7_4.2                                      updates                                 1.1 M
	glibc-headers                                  x86_64                                 2.17-196.el7_4.2                                      updates                                 676 k
	kernel-headers                                 x86_64                                 3.10.0-693.17.1.el7                                   updates                                 6.0 M
	libgomp                                        x86_64                                 4.8.5-16.el7_4.1                                      updates                                 154 k
	libmpc                                         x86_64                                 1.0.1-3.el7                                           base                                     51 k
	mpfr                                           x86_64                                 3.1.1-4.el7                                           base                                    203 k

	Transaction Summary
	===========================================================================================================================================================================================
	Install  1 Package (+7 Dependent packages)

	Total download size: 30 M
	Installed size: 60 M
	Is this ok [y/d/N]: y
	Downloading packages:
	(1/8): glibc-headers-2.17-196.el7_4.2.x86_64.rpm                                                                                                                    | 676 kB  00:00:01
	(2/8): libgomp-4.8.5-16.el7_4.1.x86_64.rpm                                                                                                                          | 154 kB  00:00:00
	(3/8): glibc-devel-2.17-196.el7_4.2.x86_64.rpm                                                                                                                      | 1.1 MB  00:00:02
	(4/8): libmpc-1.0.1-3.el7.x86_64.rpm                                                                                                                                |  51 kB  00:00:00
	(5/8): mpfr-3.1.1-4.el7.x86_64.rpm                                                                                                                                  | 203 kB  00:00:00
	(6/8): cpp-4.8.5-16.el7_4.1.x86_64.rpm                                                                                                                              | 5.9 MB  00:00:05
	(7/8): kernel-headers-3.10.0-693.17.1.el7.x86_64.rpm                                                                                                                | 6.0 MB  00:00:12
	(8/8): gcc-4.8.5-16.el7_4.1.x86_64.rpm                                                                                                                              |  16 MB  00:01:13
	-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	Total                                                                                                                                                      421 kB/s |  30 MB  00:01:13
	Running transaction check
	Running transaction test
	Transaction test succeeded
	Running transaction
	Installing : mpfr-3.1.1-4.el7.x86_64                                                                                                                                                 1/8
	Installing : libmpc-1.0.1-3.el7.x86_64                                                                                                                                               2/8
	Installing : cpp-4.8.5-16.el7_4.1.x86_64                                                                                                                                             3/8
	Installing : kernel-headers-3.10.0-693.17.1.el7.x86_64                                                                                                                               4/8
	Installing : glibc-headers-2.17-196.el7_4.2.x86_64                                                                                                                                   5/8
	Installing : glibc-devel-2.17-196.el7_4.2.x86_64                                                                                                                                     6/8
	Installing : libgomp-4.8.5-16.el7_4.1.x86_64                                                                                                                                         7/8
	Installing : gcc-4.8.5-16.el7_4.1.x86_64                                                                                                                                             8/8
	Verifying  : cpp-4.8.5-16.el7_4.1.x86_64                                                                                                                                             1/8
	Verifying  : glibc-devel-2.17-196.el7_4.2.x86_64                                                                                                                                     2/8
	Verifying  : mpfr-3.1.1-4.el7.x86_64                                                                                                                                                 3/8
	Verifying  : libgomp-4.8.5-16.el7_4.1.x86_64                                                                                                                                         4/8
	Verifying  : libmpc-1.0.1-3.el7.x86_64                                                                                                                                               5/8
	Verifying  : kernel-headers-3.10.0-693.17.1.el7.x86_64                                                                                                                               6/8
	Verifying  : glibc-headers-2.17-196.el7_4.2.x86_64                                                                                                                                   7/8
	Verifying  : gcc-4.8.5-16.el7_4.1.x86_64                                                                                                                                             8/8

	Installed:
	gcc.x86_64 0:4.8.5-16.el7_4.1

	Dependency Installed:
	cpp.x86_64 0:4.8.5-16.el7_4.1            glibc-devel.x86_64 0:2.17-196.el7_4.2        glibc-headers.x86_64 0:2.17-196.el7_4.2        kernel-headers.x86_64 0:3.10.0-693.17.1.el7
	libgomp.x86_64 0:4.8.5-16.el7_4.1        libmpc.x86_64 0:1.0.1-3.el7                  mpfr.x86_64 0:3.1.1-4.el7

	Complete!


::

    (intranet) [root@35d914e8c996 intranet]# pip install regex

::

	Collecting regex
	Using cached regex-2018.01.10.tar.gz
	Building wheels for collected packages: regex
	Running setup.py bdist_wheel for regex ... done
	Stored in directory: /root/.cache/pip/wheels/6c/44/28/d58762d1fbdf2e6f6fb00d4fec7d3384ad0ac565b895c044eb
	Successfully built regex
	Installing collected packages: regex
	Successfully installed regex-2018.1.10



yum install openldap-devel
============================

::

    (intranet) [root@35d914e8c996 intranet]# yum install openldap-devel

::

	Loaded plugins: fastestmirror, ovl
	Loading mirror speeds from cached hostfile
	* base: centos.quelquesmots.fr
	* epel: fr.mirror.babylon.network
	* extras: fr.mirror.babylon.network
	* ius: mirrors.tongji.edu.cn
	* updates: fr.mirror.babylon.network
	Resolving Dependencies
	--> Running transaction check
	---> Package openldap-devel.x86_64 0:2.4.44-5.el7 will be installed
	--> Processing Dependency: cyrus-sasl-devel(x86-64) for package: openldap-devel-2.4.44-5.el7.x86_64
	--> Running transaction check
	---> Package cyrus-sasl-devel.x86_64 0:2.1.26-21.el7 will be installed
	--> Processing Dependency: cyrus-sasl(x86-64) = 2.1.26-21.el7 for package: cyrus-sasl-devel-2.1.26-21.el7.x86_64
	--> Running transaction check
	---> Package cyrus-sasl.x86_64 0:2.1.26-21.el7 will be installed
	--> Processing Dependency: /sbin/service for package: cyrus-sasl-2.1.26-21.el7.x86_64
	--> Running transaction check
	---> Package initscripts.x86_64 0:9.49.39-1.el7_4.1 will be installed
	--> Processing Dependency: sysvinit-tools >= 2.87-5 for package: initscripts-9.49.39-1.el7_4.1.x86_64
	--> Processing Dependency: iproute for package: initscripts-9.49.39-1.el7_4.1.x86_64
	--> Running transaction check
	---> Package iproute.x86_64 0:3.10.0-87.el7 will be installed
	--> Processing Dependency: libmnl.so.0(LIBMNL_1.0)(64bit) for package: iproute-3.10.0-87.el7.x86_64
	--> Processing Dependency: libxtables.so.10()(64bit) for package: iproute-3.10.0-87.el7.x86_64
	--> Processing Dependency: libmnl.so.0()(64bit) for package: iproute-3.10.0-87.el7.x86_64
	---> Package sysvinit-tools.x86_64 0:2.88-14.dsf.el7 will be installed
	--> Running transaction check
	---> Package iptables.x86_64 0:1.4.21-18.2.el7_4 will be installed
	--> Processing Dependency: libnfnetlink.so.0()(64bit) for package: iptables-1.4.21-18.2.el7_4.x86_64
	--> Processing Dependency: libnetfilter_conntrack.so.3()(64bit) for package: iptables-1.4.21-18.2.el7_4.x86_64
	---> Package libmnl.x86_64 0:1.0.3-7.el7 will be installed
	--> Running transaction check
	---> Package libnetfilter_conntrack.x86_64 0:1.0.6-1.el7_3 will be installed
	---> Package libnfnetlink.x86_64 0:1.0.1-4.el7 will be installed
	--> Finished Dependency Resolution

	Dependencies Resolved

	===========================================================================================================================================================================================
	Package                                               Arch                                  Version                                          Repository                              Size
	===========================================================================================================================================================================================
	Installing:
	openldap-devel                                        x86_64                                2.4.44-5.el7                                     base                                   801 k
	Installing for dependencies:
	cyrus-sasl                                            x86_64                                2.1.26-21.el7                                    base                                    88 k
	cyrus-sasl-devel                                      x86_64                                2.1.26-21.el7                                    base                                   310 k
	initscripts                                           x86_64                                9.49.39-1.el7_4.1                                updates                                435 k
	iproute                                               x86_64                                3.10.0-87.el7                                    base                                   651 k
	iptables                                              x86_64                                1.4.21-18.2.el7_4                                updates                                428 k
	libmnl                                                x86_64                                1.0.3-7.el7                                      base                                    23 k
	libnetfilter_conntrack                                x86_64                                1.0.6-1.el7_3                                    base                                    55 k
	libnfnetlink                                          x86_64                                1.0.1-4.el7                                      base                                    26 k
	sysvinit-tools                                        x86_64                                2.88-14.dsf.el7                                  base                                    63 k

	Transaction Summary
	===========================================================================================================================================================================================
	Install  1 Package (+9 Dependent packages)

	Total download size: 2.8 M
	Installed size: 9.5 M
	Is this ok [y/d/N]: y
	Downloading packages:
	(1/10): cyrus-sasl-2.1.26-21.el7.x86_64.rpm                                                                                                                         |  88 kB  00:00:00
	(2/10): cyrus-sasl-devel-2.1.26-21.el7.x86_64.rpm                                                                                                                   | 310 kB  00:00:00
	(3/10): libmnl-1.0.3-7.el7.x86_64.rpm                                                                                                                               |  23 kB  00:00:00
	(4/10): initscripts-9.49.39-1.el7_4.1.x86_64.rpm                                                                                                                    | 435 kB  00:00:00
	(5/10): libnetfilter_conntrack-1.0.6-1.el7_3.x86_64.rpm                                                                                                             |  55 kB  00:00:00
	(6/10): libnfnetlink-1.0.1-4.el7.x86_64.rpm                                                                                                                         |  26 kB  00:00:00
	(7/10): iptables-1.4.21-18.2.el7_4.x86_64.rpm                                                                                                                       | 428 kB  00:00:01
	(8/10): sysvinit-tools-2.88-14.dsf.el7.x86_64.rpm                                                                                                                   |  63 kB  00:00:00
	(9/10): openldap-devel-2.4.44-5.el7.x86_64.rpm                                                                                                                      | 801 kB  00:00:00
	(10/10): iproute-3.10.0-87.el7.x86_64.rpm                                                                                                                           | 651 kB  00:00:01
	-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	Total                                                                                                                                                      1.2 MB/s | 2.8 MB  00:00:02
	Running transaction check
	Running transaction test
	Transaction test succeeded
	Running transaction
	Installing : libnfnetlink-1.0.1-4.el7.x86_64                                                                                                                                        1/10
	Installing : libmnl-1.0.3-7.el7.x86_64                                                                                                                                              2/10
	Installing : libnetfilter_conntrack-1.0.6-1.el7_3.x86_64                                                                                                                            3/10
	Installing : iptables-1.4.21-18.2.el7_4.x86_64                                                                                                                                      4/10
	Installing : iproute-3.10.0-87.el7.x86_64                                                                                                                                           5/10
	Installing : sysvinit-tools-2.88-14.dsf.el7.x86_64                                                                                                                                  6/10
	Installing : initscripts-9.49.39-1.el7_4.1.x86_64                                                                                                                                   7/10
	Installing : cyrus-sasl-2.1.26-21.el7.x86_64                                                                                                                                        8/10
	Installing : cyrus-sasl-devel-2.1.26-21.el7.x86_64                                                                                                                                  9/10
	Installing : openldap-devel-2.4.44-5.el7.x86_64                                                                                                                                    10/10
	Verifying  : iptables-1.4.21-18.2.el7_4.x86_64                                                                                                                                      1/10
	Verifying  : libmnl-1.0.3-7.el7.x86_64                                                                                                                                              2/10
	Verifying  : iproute-3.10.0-87.el7.x86_64                                                                                                                                           3/10
	Verifying  : initscripts-9.49.39-1.el7_4.1.x86_64                                                                                                                                   4/10
	Verifying  : cyrus-sasl-devel-2.1.26-21.el7.x86_64                                                                                                                                  5/10
	Verifying  : libnfnetlink-1.0.1-4.el7.x86_64                                                                                                                                        6/10
	Verifying  : sysvinit-tools-2.88-14.dsf.el7.x86_64                                                                                                                                  7/10
	Verifying  : libnetfilter_conntrack-1.0.6-1.el7_3.x86_64                                                                                                                            8/10
	Verifying  : openldap-devel-2.4.44-5.el7.x86_64                                                                                                                                     9/10
	Verifying  : cyrus-sasl-2.1.26-21.el7.x86_64                                                                                                                                       10/10

	Installed:
	openldap-devel.x86_64 0:2.4.44-5.el7

	Dependency Installed:
	cyrus-sasl.x86_64 0:2.1.26-21.el7             cyrus-sasl-devel.x86_64 0:2.1.26-21.el7       initscripts.x86_64 0:9.49.39-1.el7_4.1              iproute.x86_64 0:3.10.0-87.el7
	iptables.x86_64 0:1.4.21-18.2.el7_4           libmnl.x86_64 0:1.0.3-7.el7                   libnetfilter_conntrack.x86_64 0:1.0.6-1.el7_3       libnfnetlink.x86_64 0:1.0.1-4.el7
	sysvinit-tools.x86_64 0:2.88-14.dsf.el7

	Complete!


pip install pyldap
===================

::

    (intranet) [root@35d914e8c996 intranet]# pip install pyldap

::

	Collecting pyldap
	Using cached pyldap-2.4.45.tar.gz
	Requirement already satisfied: setuptools in ./intranet/lib/python3.6/site-packages (from pyldap)
	Building wheels for collected packages: pyldap
	Running setup.py bdist_wheel for pyldap ... done
	Stored in directory: /root/.cache/pip/wheels/0c/a3/42/e6127de64a53567a11c4e3ee5991547cb8f5a3241d2d67947e
	Successfully built pyldap
	Installing collected packages: pyldap
	Successfully installed pyldap-2.4.45


Nouveau fichier Dockerfile
============================


Dockerfile
-----------


::

	# Use an official centos7 image
	FROM centos:7

	RUN yum update -y \
		&& yum install -y https://centos7.iuscommunity.org/ius-release.rpm \
		&& yum install -y python36u python36u-libs python36u-devel python36u-pip \
		&& yum install -y which gcc \ # we need regex and pyldap
		&& yum install -y openldap-devel  # we need pyldap

which python3.6
----------------

::

    [root@5a070209b99d /]# which python3.6

::

    /usr/bin/python3.6



python3.6 -m pip install pipenv
---------------------------------

::

    python3.6 -m pip install pipenv


::


	Collecting pipenv
	  Downloading pipenv-9.0.3.tar.gz (3.9MB)
		100% |████████████████████████████████| 3.9MB 336kB/s
	Collecting virtualenv (from pipenv)
	  Downloading virtualenv-15.1.0-py2.py3-none-any.whl (1.8MB)
		100% |████████████████████████████████| 1.8MB 602kB/s
	Collecting pew>=0.1.26 (from pipenv)
	  Downloading pew-1.1.2-py2.py3-none-any.whl
	Requirement already satisfied: pip>=9.0.1 in /usr/lib/python3.6/site-packages (from pipenv)
	Collecting requests>2.18.0 (from pipenv)
	  Downloading requests-2.18.4-py2.py3-none-any.whl (88kB)
		100% |████████████████████████████████| 92kB 2.2MB/s
	Collecting flake8>=3.0.0 (from pipenv)
	  Downloading flake8-3.5.0-py2.py3-none-any.whl (69kB)
		100% |████████████████████████████████| 71kB 1.8MB/s
	Collecting urllib3>=1.21.1 (from pipenv)
	  Downloading urllib3-1.22-py2.py3-none-any.whl (132kB)
		100% |████████████████████████████████| 133kB 1.8MB/s
	Requirement already satisfied: setuptools>=17.1 in /usr/lib/python3.6/site-packages (from pew>=0.1.26->pipenv)
	Collecting virtualenv-clone>=0.2.5 (from pew>=0.1.26->pipenv)
	  Downloading virtualenv-clone-0.2.6.tar.gz
	Collecting certifi>=2017.4.17 (from requests>2.18.0->pipenv)
	  Downloading certifi-2018.1.18-py2.py3-none-any.whl (151kB)
		100% |████████████████████████████████| 153kB 982kB/s
	Collecting chardet<3.1.0,>=3.0.2 (from requests>2.18.0->pipenv)
	  Downloading chardet-3.0.4-py2.py3-none-any.whl (133kB)
		100% |████████████████████████████████| 143kB 1.8MB/s
	Collecting idna<2.7,>=2.5 (from requests>2.18.0->pipenv)
	  Downloading idna-2.6-py2.py3-none-any.whl (56kB)
		100% |████████████████████████████████| 61kB 900kB/s
	Collecting mccabe<0.7.0,>=0.6.0 (from flake8>=3.0.0->pipenv)
	  Downloading mccabe-0.6.1-py2.py3-none-any.whl
	Collecting pycodestyle<2.4.0,>=2.0.0 (from flake8>=3.0.0->pipenv)
	  Downloading pycodestyle-2.3.1-py2.py3-none-any.whl (45kB)
		100% |████████████████████████████████| 51kB 2.3MB/s
	Collecting pyflakes<1.7.0,>=1.5.0 (from flake8>=3.0.0->pipenv)
	  Downloading pyflakes-1.6.0-py2.py3-none-any.whl (227kB)
		100% |████████████████████████████████| 235kB 2.2MB/s
	Installing collected packages: virtualenv, virtualenv-clone, pew, urllib3, certifi, chardet, idna, requests, mccabe, pycodestyle, pyflakes, flake8, pipenv
	  Running setup.py install for virtualenv-clone ... done
	  Running setup.py install for pipenv ... done
	Successfully installed certifi-2018.1.18 chardet-3.0.4 flake8-3.5.0 idna-2.6 mccabe-0.6.1 pew-1.1.2 pipenv-9.0.3 pycodestyle-2.3.1 pyflakes-1.6.0 requests-2.18.4 urllib3-1.22 virtualenv-15.1.0 virtualenv-clone-0.2.6



Nouveau Dockerfile
===================

Dockerfile
-----------


::

	# Use an official centos7 image
	FROM centos:7

	RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8
	ENV LANG fr_FR.utf8

	# gcc because we need regex and pyldap
	# openldap-devel because we need pyldap
	RUN yum update -y \
		&& yum install -y https://centos7.iuscommunity.org/ius-release.rpm \
		&& yum install -y python36u python36u-libs python36u-devel python36u-pip \
		&& yum install -y which gcc \
		&& yum install -y openldap-devel

	RUN python3.6 -m pip install pipenv

docker build -t id3centos7:0.1.1 .
--------------------------------------

::

    PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\centos7> docker build -t id3centos7:0.1.1 .

::

	Sending build context to Docker daemon  90.11kB
	Step 1/5 : FROM centos:7
	---> ff426288ea90
	Step 2/5 : RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8
	---> Running in b90f824550e7
	Removing intermediate container b90f824550e7
	---> b7dac1f044e3
	Step 3/5 : ENV LANG fr_FR.utf8
	---> Running in 107f8edaf492
	Removing intermediate container 107f8edaf492
	---> e28a88050b8f
	Step 4/5 : RUN yum update -y     && yum install -y https://centos7.iuscommunity.org/ius-release.rpm     && yum install -y python36u python36u-libs python36u-devel python36u-pip     && yum install -y which gcc     && yum install -y openldap-devel
	---> Running in 531a6dcb0ab1
	Loaded plugins: fastestmirror, ovl
	Determining fastest mirrors
	* base: centos.quelquesmots.fr
	* extras: ftp.ciril.fr
	* updates: centos.quelquesmots.fr
	Resolving Dependencies
	--> Running transaction check
	---> Package bind-license.noarch 32:9.9.4-51.el7_4.1 will be updated
	---> Package bind-license.noarch 32:9.9.4-51.el7_4.2 will be an update
	---> Package binutils.x86_64 0:2.25.1-32.base.el7_4.1 will be updated
	---> Package binutils.x86_64 0:2.25.1-32.base.el7_4.2 will be an update
	---> Package kmod.x86_64 0:20-15.el7_4.6 will be updated
	---> Package kmod.x86_64 0:20-15.el7_4.7 will be an update
	---> Package kmod-libs.x86_64 0:20-15.el7_4.6 will be updated
	---> Package kmod-libs.x86_64 0:20-15.el7_4.7 will be an update
	---> Package kpartx.x86_64 0:0.4.9-111.el7 will be updated
	---> Package kpartx.x86_64 0:0.4.9-111.el7_4.2 will be an update
	---> Package libdb.x86_64 0:5.3.21-20.el7 will be updated
	---> Package libdb.x86_64 0:5.3.21-21.el7_4 will be an update
	---> Package libdb-utils.x86_64 0:5.3.21-20.el7 will be updated
	---> Package libdb-utils.x86_64 0:5.3.21-21.el7_4 will be an update
	---> Package systemd.x86_64 0:219-42.el7_4.4 will be updated
	---> Package systemd.x86_64 0:219-42.el7_4.7 will be an update
	---> Package systemd-libs.x86_64 0:219-42.el7_4.4 will be updated
	---> Package systemd-libs.x86_64 0:219-42.el7_4.7 will be an update
	---> Package tzdata.noarch 0:2017c-1.el7 will be updated
	---> Package tzdata.noarch 0:2018c-1.el7 will be an update
	---> Package yum.noarch 0:3.4.3-154.el7.centos will be updated
	---> Package yum.noarch 0:3.4.3-154.el7.centos.1 will be an update
	--> Finished Dependency Resolution

	Dependencies Resolved

	================================================================================
	Package           Arch        Version                       Repository    Size
	================================================================================
	Updating:
	bind-license      noarch      32:9.9.4-51.el7_4.2           updates       84 k
	binutils          x86_64      2.25.1-32.base.el7_4.2        updates      5.4 M
	kmod              x86_64      20-15.el7_4.7                 updates      121 k
	kmod-libs         x86_64      20-15.el7_4.7                 updates       50 k
	kpartx            x86_64      0.4.9-111.el7_4.2             updates       73 k
	libdb             x86_64      5.3.21-21.el7_4               updates      719 k
	libdb-utils       x86_64      5.3.21-21.el7_4               updates      132 k
	systemd           x86_64      219-42.el7_4.7                updates      5.2 M
	systemd-libs      x86_64      219-42.el7_4.7                updates      376 k
	tzdata            noarch      2018c-1.el7                   updates      479 k
	yum               noarch      3.4.3-154.el7.centos.1        updates      1.2 M

	Transaction Summary
	================================================================================
	Upgrade  11 Packages

	Total download size: 14 M
	Downloading packages:
	Delta RPMs disabled because /usr/bin/applydeltarpm not installed.
	warning: /var/cache/yum/x86_64/7/updates/packages/bind-license-9.9.4-51.el7_4.2.noarch.rpm: Header V3 RSA/SHA256 Signature, key ID f4a80eb5: NOKEY
	Public key for bind-license-9.9.4-51.el7_4.2.noarch.rpm is not installed
	--------------------------------------------------------------------------------
	Total                                              1.5 MB/s |  14 MB  00:09
	Retrieving key from file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
	Importing GPG key 0xF4A80EB5:
	Userid     : "CentOS-7 Key (CentOS 7 Official Signing Key) <security@centos.org>"
	Fingerprint: 6341 ab27 53d7 8a78 a7c2 7bb1 24c6 a8a7 f4a8 0eb5
	Package    : centos-release-7-4.1708.el7.centos.x86_64 (@CentOS)
	From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
	Running transaction check
	Running transaction test
	Transaction test succeeded
	Running transaction
	Updating   : libdb-5.3.21-21.el7_4.x86_64                                1/22
	Updating   : binutils-2.25.1-32.base.el7_4.2.x86_64                      2/22
	Updating   : kmod-20-15.el7_4.7.x86_64                                   3/22
	Updating   : systemd-libs-219-42.el7_4.7.x86_64                          4/22
	Updating   : kmod-libs-20-15.el7_4.7.x86_64                              5/22
	Updating   : systemd-219-42.el7_4.7.x86_64                               6/22
	Updating   : libdb-utils-5.3.21-21.el7_4.x86_64                          7/22
	Updating   : yum-3.4.3-154.el7.centos.1.noarch                           8/22
	Updating   : 32:bind-license-9.9.4-51.el7_4.2.noarch                     9/22
	Updating   : tzdata-2018c-1.el7.noarch                                  10/22
	Updating   : kpartx-0.4.9-111.el7_4.2.x86_64                            11/22
	Cleanup    : systemd-219-42.el7_4.4.x86_64                              12/22
	Cleanup    : kmod-20-15.el7_4.6.x86_64                                  13/22
	Cleanup    : libdb-utils-5.3.21-20.el7.x86_64                           14/22
	Cleanup    : yum-3.4.3-154.el7.centos.noarch                            15/22
	Cleanup    : 32:bind-license-9.9.4-51.el7_4.1.noarch                    16/22
	Cleanup    : tzdata-2017c-1.el7.noarch                                  17/22
	Cleanup    : libdb-5.3.21-20.el7.x86_64                                 18/22
	Cleanup    : binutils-2.25.1-32.base.el7_4.1.x86_64                     19/22
	Cleanup    : kmod-libs-20-15.el7_4.6.x86_64                             20/22
	Cleanup    : systemd-libs-219-42.el7_4.4.x86_64                         21/22
	Cleanup    : kpartx-0.4.9-111.el7.x86_64                                22/22
	Verifying  : kmod-20-15.el7_4.7.x86_64                                   1/22
	Verifying  : kmod-libs-20-15.el7_4.7.x86_64                              2/22
	Verifying  : libdb-utils-5.3.21-21.el7_4.x86_64                          3/22
	Verifying  : systemd-219-42.el7_4.7.x86_64                               4/22
	Verifying  : kpartx-0.4.9-111.el7_4.2.x86_64                             5/22
	Verifying  : tzdata-2018c-1.el7.noarch                                   6/22
	Verifying  : 32:bind-license-9.9.4-51.el7_4.2.noarch                     7/22
	Verifying  : systemd-libs-219-42.el7_4.7.x86_64                          8/22
	Verifying  : binutils-2.25.1-32.base.el7_4.2.x86_64                      9/22
	Verifying  : libdb-5.3.21-21.el7_4.x86_64                               10/22
	Verifying  : yum-3.4.3-154.el7.centos.1.noarch                          11/22
	Verifying  : binutils-2.25.1-32.base.el7_4.1.x86_64                     12/22
	Verifying  : 32:bind-license-9.9.4-51.el7_4.1.noarch                    13/22
	Verifying  : systemd-libs-219-42.el7_4.4.x86_64                         14/22
	Verifying  : kmod-20-15.el7_4.6.x86_64                                  15/22
	Verifying  : systemd-219-42.el7_4.4.x86_64                              16/22
	Verifying  : libdb-utils-5.3.21-20.el7.x86_64                           17/22
	Verifying  : kmod-libs-20-15.el7_4.6.x86_64                             18/22
	Verifying  : tzdata-2017c-1.el7.noarch                                  19/22
	Verifying  : kpartx-0.4.9-111.el7.x86_64                                20/22
	Verifying  : yum-3.4.3-154.el7.centos.noarch                            21/22
	Verifying  : libdb-5.3.21-20.el7.x86_64                                 22/22

	Updated:
	bind-license.noarch 32:9.9.4-51.el7_4.2
	binutils.x86_64 0:2.25.1-32.base.el7_4.2
	kmod.x86_64 0:20-15.el7_4.7
	kmod-libs.x86_64 0:20-15.el7_4.7
	kpartx.x86_64 0:0.4.9-111.el7_4.2
	libdb.x86_64 0:5.3.21-21.el7_4
	libdb-utils.x86_64 0:5.3.21-21.el7_4
	systemd.x86_64 0:219-42.el7_4.7
	systemd-libs.x86_64 0:219-42.el7_4.7
	tzdata.noarch 0:2018c-1.el7
	yum.noarch 0:3.4.3-154.el7.centos.1

	Complete!
	Loaded plugins: fastestmirror, ovl
	Examining /var/tmp/yum-root-CU9Amb/ius-release.rpm: ius-release-1.0-15.ius.centos7.noarch
	Marking /var/tmp/yum-root-CU9Amb/ius-release.rpm to be installed
	Resolving Dependencies
	--> Running transaction check
	---> Package ius-release.noarch 0:1.0-15.ius.centos7 will be installed
	--> Processing Dependency: epel-release = 7 for package: ius-release-1.0-15.ius.centos7.noarch
	Loading mirror speeds from cached hostfile
	* base: centos.quelquesmots.fr
	* extras: ftp.ciril.fr
	* updates: centos.quelquesmots.fr
	--> Running transaction check
	---> Package epel-release.noarch 0:7-9 will be installed
	--> Finished Dependency Resolution

	Dependencies Resolved

	================================================================================
	Package           Arch        Version                  Repository         Size
	================================================================================
	Installing:
	ius-release       noarch      1.0-15.ius.centos7       /ius-release      8.5 k
	Installing for dependencies:
	epel-release      noarch      7-9                      extras             14 k

	Transaction Summary
	================================================================================
	Install  1 Package (+1 Dependent package)

	Total size: 23 k
	Total download size: 14 k
	Installed size: 33 k
	Downloading packages:
	Running transaction check
	Running transaction test
	Transaction test succeeded
	Running transaction
	Installing : epel-release-7-9.noarch                                      1/2
	Installing : ius-release-1.0-15.ius.centos7.noarch                        2/2
	Verifying  : ius-release-1.0-15.ius.centos7.noarch                        1/2
	Verifying  : epel-release-7-9.noarch                                      2/2

	Installed:
	ius-release.noarch 0:1.0-15.ius.centos7

	Dependency Installed:
	epel-release.noarch 0:7-9

	Complete!
	Loaded plugins: fastestmirror, ovl
	Loading mirror speeds from cached hostfile
	* base: centos.quelquesmots.fr
	* epel: fr.mirror.babylon.network
	* extras: ftp.ciril.fr
	* ius: mirrors.ircam.fr
	* updates: centos.quelquesmots.fr
	Resolving Dependencies
	--> Running transaction check
	---> Package python36u.x86_64 0:3.6.4-1.ius.centos7 will be installed
	---> Package python36u-devel.x86_64 0:3.6.4-1.ius.centos7 will be installed
	---> Package python36u-libs.x86_64 0:3.6.4-1.ius.centos7 will be installed
	---> Package python36u-pip.noarch 0:9.0.1-1.ius.centos7 will be installed
	--> Processing Dependency: python36u-setuptools for package: python36u-pip-9.0.1-1.ius.centos7.noarch
	--> Running transaction check
	---> Package python36u-setuptools.noarch 0:36.6.0-1.ius.centos7 will be installed
	--> Finished Dependency Resolution

	Dependencies Resolved

	================================================================================
	Package                   Arch        Version                   Repository
																		   Size
	================================================================================
	Installing:
	python36u                 x86_64      3.6.4-1.ius.centos7       ius       56 k
	python36u-devel           x86_64      3.6.4-1.ius.centos7       ius      839 k
	python36u-libs            x86_64      3.6.4-1.ius.centos7       ius      8.7 M
	python36u-pip             noarch      9.0.1-1.ius.centos7       ius      1.8 M
	Installing for dependencies:
	python36u-setuptools      noarch      36.6.0-1.ius.centos7      ius      587 k

	Transaction Summary
	================================================================================
	Install  4 Packages (+1 Dependent package)

	Total download size: 12 M
	Installed size: 53 M
	Downloading packages:
	warning: /var/cache/yum/x86_64/7/ius/packages/python36u-setuptools-36.6.0-1.ius.centos7.noarch.rpm: Header V4 DSA/SHA1 Signature, key ID 9cd4953f: NOKEY
	Public key for python36u-setuptools-36.6.0-1.ius.centos7.noarch.rpm is not installed
	--------------------------------------------------------------------------------
	Total                                              634 kB/s |  12 MB  00:19
	Retrieving key from file:///etc/pki/rpm-gpg/IUS-COMMUNITY-GPG-KEY
	Importing GPG key 0x9CD4953F:
	Userid     : "IUS Community Project <coredev@iuscommunity.org>"
	Fingerprint: 8b84 6e3a b3fe 6462 74e8 670f da22 1cdf 9cd4 953f
	Package    : ius-release-1.0-15.ius.centos7.noarch (installed)
	From       : /etc/pki/rpm-gpg/IUS-COMMUNITY-GPG-KEY
	Running transaction check
	Running transaction test
	Transaction test succeeded
	Running transaction
	Installing : python36u-libs-3.6.4-1.ius.centos7.x86_64                    1/5
	Installing : python36u-3.6.4-1.ius.centos7.x86_64                         2/5
	Installing : python36u-setuptools-36.6.0-1.ius.centos7.noarch             3/5
	Installing : python36u-pip-9.0.1-1.ius.centos7.noarch                     4/5
	Installing : python36u-devel-3.6.4-1.ius.centos7.x86_64                   5/5
	Verifying  : python36u-setuptools-36.6.0-1.ius.centos7.noarch             1/5
	Verifying  : python36u-pip-9.0.1-1.ius.centos7.noarch                     2/5
	Verifying  : python36u-3.6.4-1.ius.centos7.x86_64                         3/5
	Verifying  : python36u-libs-3.6.4-1.ius.centos7.x86_64                    4/5
	Verifying  : python36u-devel-3.6.4-1.ius.centos7.x86_64                   5/5

	Installed:
	python36u.x86_64 0:3.6.4-1.ius.centos7
	python36u-devel.x86_64 0:3.6.4-1.ius.centos7
	python36u-libs.x86_64 0:3.6.4-1.ius.centos7
	python36u-pip.noarch 0:9.0.1-1.ius.centos7

	Dependency Installed:
	python36u-setuptools.noarch 0:36.6.0-1.ius.centos7

	Complete!
	Loaded plugins: fastestmirror, ovl
	Loading mirror speeds from cached hostfile
	* base: centos.quelquesmots.fr
	* epel: fr.mirror.babylon.network
	* extras: ftp.ciril.fr
	* ius: mirrors.ircam.fr
	* updates: centos.quelquesmots.fr
	Resolving Dependencies
	--> Running transaction check
	---> Package gcc.x86_64 0:4.8.5-16.el7_4.1 will be installed
	--> Processing Dependency: libgomp = 4.8.5-16.el7_4.1 for package: gcc-4.8.5-16.el7_4.1.x86_64
	--> Processing Dependency: cpp = 4.8.5-16.el7_4.1 for package: gcc-4.8.5-16.el7_4.1.x86_64
	--> Processing Dependency: glibc-devel >= 2.2.90-12 for package: gcc-4.8.5-16.el7_4.1.x86_64
	--> Processing Dependency: libmpfr.so.4()(64bit) for package: gcc-4.8.5-16.el7_4.1.x86_64
	--> Processing Dependency: libmpc.so.3()(64bit) for package: gcc-4.8.5-16.el7_4.1.x86_64
	--> Processing Dependency: libgomp.so.1()(64bit) for package: gcc-4.8.5-16.el7_4.1.x86_64
	---> Package which.x86_64 0:2.20-7.el7 will be installed
	--> Running transaction check
	---> Package cpp.x86_64 0:4.8.5-16.el7_4.1 will be installed
	---> Package glibc-devel.x86_64 0:2.17-196.el7_4.2 will be installed
	--> Processing Dependency: glibc-headers = 2.17-196.el7_4.2 for package: glibc-devel-2.17-196.el7_4.2.x86_64
	--> Processing Dependency: glibc-headers for package: glibc-devel-2.17-196.el7_4.2.x86_64
	---> Package libgomp.x86_64 0:4.8.5-16.el7_4.1 will be installed
	---> Package libmpc.x86_64 0:1.0.1-3.el7 will be installed
	---> Package mpfr.x86_64 0:3.1.1-4.el7 will be installed
	--> Running transaction check
	---> Package glibc-headers.x86_64 0:2.17-196.el7_4.2 will be installed
	--> Processing Dependency: kernel-headers >= 2.2.1 for package: glibc-headers-2.17-196.el7_4.2.x86_64
	--> Processing Dependency: kernel-headers for package: glibc-headers-2.17-196.el7_4.2.x86_64
	--> Running transaction check
	---> Package kernel-headers.x86_64 0:3.10.0-693.17.1.el7 will be installed
	--> Finished Dependency Resolution

	Dependencies Resolved

	================================================================================
	Package             Arch        Version                     Repository    Size
	================================================================================
	Installing:
	gcc                 x86_64      4.8.5-16.el7_4.1            updates       16 M
	which               x86_64      2.20-7.el7                  base          41 k
	Installing for dependencies:
	cpp                 x86_64      4.8.5-16.el7_4.1            updates      5.9 M
	glibc-devel         x86_64      2.17-196.el7_4.2            updates      1.1 M
	glibc-headers       x86_64      2.17-196.el7_4.2            updates      676 k
	kernel-headers      x86_64      3.10.0-693.17.1.el7         updates      6.0 M
	libgomp             x86_64      4.8.5-16.el7_4.1            updates      154 k
	libmpc              x86_64      1.0.1-3.el7                 base          51 k
	mpfr                x86_64      3.1.1-4.el7                 base         203 k

	Transaction Summary
	================================================================================
	Install  2 Packages (+7 Dependent packages)

	Total download size: 30 M
	Installed size: 60 M
	Downloading packages:
	--------------------------------------------------------------------------------
	Total                                              1.3 MB/s |  30 MB  00:23
	Running transaction check
	Running transaction test
	Transaction test succeeded
	Running transaction
	Installing : mpfr-3.1.1-4.el7.x86_64                                      1/9
	Installing : libmpc-1.0.1-3.el7.x86_64                                    2/9
	Installing : cpp-4.8.5-16.el7_4.1.x86_64                                  3/9
	Installing : kernel-headers-3.10.0-693.17.1.el7.x86_64                    4/9
	Installing : glibc-headers-2.17-196.el7_4.2.x86_64                        5/9
	Installing : glibc-devel-2.17-196.el7_4.2.x86_64                          6/9
	Installing : libgomp-4.8.5-16.el7_4.1.x86_64                              7/9
	Installing : gcc-4.8.5-16.el7_4.1.x86_64                                  8/9
	Installing : which-2.20-7.el7.x86_64                                      9/9
	install-info: No such file or directory for /usr/share/info/which.info.gz
	Verifying  : cpp-4.8.5-16.el7_4.1.x86_64                                  1/9
	Verifying  : glibc-devel-2.17-196.el7_4.2.x86_64                          2/9
	Verifying  : which-2.20-7.el7.x86_64                                      3/9
	Verifying  : mpfr-3.1.1-4.el7.x86_64                                      4/9
	Verifying  : libgomp-4.8.5-16.el7_4.1.x86_64                              5/9
	Verifying  : libmpc-1.0.1-3.el7.x86_64                                    6/9
	Verifying  : kernel-headers-3.10.0-693.17.1.el7.x86_64                    7/9
	Verifying  : glibc-headers-2.17-196.el7_4.2.x86_64                        8/9
	Verifying  : gcc-4.8.5-16.el7_4.1.x86_64                                  9/9

	Installed:
	gcc.x86_64 0:4.8.5-16.el7_4.1            which.x86_64 0:2.20-7.el7

	Dependency Installed:
	cpp.x86_64 0:4.8.5-16.el7_4.1
	glibc-devel.x86_64 0:2.17-196.el7_4.2
	glibc-headers.x86_64 0:2.17-196.el7_4.2
	kernel-headers.x86_64 0:3.10.0-693.17.1.el7
	libgomp.x86_64 0:4.8.5-16.el7_4.1
	libmpc.x86_64 0:1.0.1-3.el7
	mpfr.x86_64 0:3.1.1-4.el7

	Complete!
	Loaded plugins: fastestmirror, ovl
	Loading mirror speeds from cached hostfile
	* base: centos.quelquesmots.fr
	* epel: fr.mirror.babylon.network
	* extras: ftp.ciril.fr
	* ius: mirrors.ircam.fr
	* updates: centos.quelquesmots.fr
	Resolving Dependencies
	--> Running transaction check
	---> Package openldap-devel.x86_64 0:2.4.44-5.el7 will be installed
	--> Processing Dependency: cyrus-sasl-devel(x86-64) for package: openldap-devel-2.4.44-5.el7.x86_64
	--> Running transaction check
	---> Package cyrus-sasl-devel.x86_64 0:2.1.26-21.el7 will be installed
	--> Processing Dependency: cyrus-sasl(x86-64) = 2.1.26-21.el7 for package: cyrus-sasl-devel-2.1.26-21.el7.x86_64
	--> Running transaction check
	---> Package cyrus-sasl.x86_64 0:2.1.26-21.el7 will be installed
	--> Processing Dependency: /sbin/service for package: cyrus-sasl-2.1.26-21.el7.x86_64
	--> Running transaction check
	---> Package initscripts.x86_64 0:9.49.39-1.el7_4.1 will be installed
	--> Processing Dependency: sysvinit-tools >= 2.87-5 for package: initscripts-9.49.39-1.el7_4.1.x86_64
	--> Processing Dependency: iproute for package: initscripts-9.49.39-1.el7_4.1.x86_64
	--> Running transaction check
	---> Package iproute.x86_64 0:3.10.0-87.el7 will be installed
	--> Processing Dependency: libmnl.so.0(LIBMNL_1.0)(64bit) for package: iproute-3.10.0-87.el7.x86_64
	--> Processing Dependency: libxtables.so.10()(64bit) for package: iproute-3.10.0-87.el7.x86_64
	--> Processing Dependency: libmnl.so.0()(64bit) for package: iproute-3.10.0-87.el7.x86_64
	---> Package sysvinit-tools.x86_64 0:2.88-14.dsf.el7 will be installed
	--> Running transaction check
	---> Package iptables.x86_64 0:1.4.21-18.2.el7_4 will be installed
	--> Processing Dependency: libnfnetlink.so.0()(64bit) for package: iptables-1.4.21-18.2.el7_4.x86_64
	--> Processing Dependency: libnetfilter_conntrack.so.3()(64bit) for package: iptables-1.4.21-18.2.el7_4.x86_64
	---> Package libmnl.x86_64 0:1.0.3-7.el7 will be installed
	--> Running transaction check
	---> Package libnetfilter_conntrack.x86_64 0:1.0.6-1.el7_3 will be installed
	---> Package libnfnetlink.x86_64 0:1.0.1-4.el7 will be installed
	--> Finished Dependency Resolution

	Dependencies Resolved

	================================================================================
	Package                    Arch       Version                Repository   Size
	================================================================================
	Installing:
	openldap-devel             x86_64     2.4.44-5.el7           base        801 k
	Installing for dependencies:
	cyrus-sasl                 x86_64     2.1.26-21.el7          base         88 k
	cyrus-sasl-devel           x86_64     2.1.26-21.el7          base        310 k
	initscripts                x86_64     9.49.39-1.el7_4.1      updates     435 k
	iproute                    x86_64     3.10.0-87.el7          base        651 k
	iptables                   x86_64     1.4.21-18.2.el7_4      updates     428 k
	libmnl                     x86_64     1.0.3-7.el7            base         23 k
	libnetfilter_conntrack     x86_64     1.0.6-1.el7_3          base         55 k
	libnfnetlink               x86_64     1.0.1-4.el7            base         26 k
	sysvinit-tools             x86_64     2.88-14.dsf.el7        base         63 k

	Transaction Summary
	================================================================================
	Install  1 Package (+9 Dependent packages)

	Total download size: 2.8 M
	Installed size: 9.5 M
	Downloading packages:
	--------------------------------------------------------------------------------
	Total                                              1.2 MB/s | 2.8 MB  00:02
	Running transaction check
	Running transaction test
	Transaction test succeeded
	Running transaction
	Installing : libnfnetlink-1.0.1-4.el7.x86_64                             1/10
	Installing : libmnl-1.0.3-7.el7.x86_64                                   2/10
	Installing : libnetfilter_conntrack-1.0.6-1.el7_3.x86_64                 3/10
	Installing : iptables-1.4.21-18.2.el7_4.x86_64                           4/10
	Installing : iproute-3.10.0-87.el7.x86_64                                5/10
	Installing : sysvinit-tools-2.88-14.dsf.el7.x86_64                       6/10
	Installing : initscripts-9.49.39-1.el7_4.1.x86_64                        7/10
	Installing : cyrus-sasl-2.1.26-21.el7.x86_64                             8/10
	Installing : cyrus-sasl-devel-2.1.26-21.el7.x86_64                       9/10
	Installing : openldap-devel-2.4.44-5.el7.x86_64                         10/10
	Verifying  : iptables-1.4.21-18.2.el7_4.x86_64                           1/10
	Verifying  : libmnl-1.0.3-7.el7.x86_64                                   2/10
	Verifying  : iproute-3.10.0-87.el7.x86_64                                3/10
	Verifying  : initscripts-9.49.39-1.el7_4.1.x86_64                        4/10
	Verifying  : cyrus-sasl-devel-2.1.26-21.el7.x86_64                       5/10
	Verifying  : libnfnetlink-1.0.1-4.el7.x86_64                             6/10
	Verifying  : sysvinit-tools-2.88-14.dsf.el7.x86_64                       7/10
	Verifying  : libnetfilter_conntrack-1.0.6-1.el7_3.x86_64                 8/10
	Verifying  : openldap-devel-2.4.44-5.el7.x86_64                          9/10
	Verifying  : cyrus-sasl-2.1.26-21.el7.x86_64                            10/10

	Installed:
	openldap-devel.x86_64 0:2.4.44-5.el7

	Dependency Installed:
	cyrus-sasl.x86_64 0:2.1.26-21.el7
	cyrus-sasl-devel.x86_64 0:2.1.26-21.el7
	initscripts.x86_64 0:9.49.39-1.el7_4.1
	iproute.x86_64 0:3.10.0-87.el7
	iptables.x86_64 0:1.4.21-18.2.el7_4
	libmnl.x86_64 0:1.0.3-7.el7
	libnetfilter_conntrack.x86_64 0:1.0.6-1.el7_3
	libnfnetlink.x86_64 0:1.0.1-4.el7
	sysvinit-tools.x86_64 0:2.88-14.dsf.el7

	Complete!
	Removing intermediate container 531a6dcb0ab1
	---> 0cfdf4200049
	Step 5/5 : RUN python3.6 -m pip install pipenv
	---> Running in 222c51c8c187
	Collecting pipenv
	Downloading pipenv-9.0.3.tar.gz (3.9MB)
	Collecting virtualenv (from pipenv)
	Downloading virtualenv-15.1.0-py2.py3-none-any.whl (1.8MB)
	Collecting pew>=0.1.26 (from pipenv)
	Downloading pew-1.1.2-py2.py3-none-any.whl
	Requirement already satisfied: pip>=9.0.1 in /usr/lib/python3.6/site-packages (from pipenv)
	Collecting requests>2.18.0 (from pipenv)
	Downloading requests-2.18.4-py2.py3-none-any.whl (88kB)
	Collecting flake8>=3.0.0 (from pipenv)
	Downloading flake8-3.5.0-py2.py3-none-any.whl (69kB)
	Collecting urllib3>=1.21.1 (from pipenv)
	Downloading urllib3-1.22-py2.py3-none-any.whl (132kB)
	Requirement already satisfied: setuptools>=17.1 in /usr/lib/python3.6/site-packages (from pew>=0.1.26->pipenv)
	Collecting virtualenv-clone>=0.2.5 (from pew>=0.1.26->pipenv)
	Downloading virtualenv-clone-0.2.6.tar.gz
	Collecting certifi>=2017.4.17 (from requests>2.18.0->pipenv)
	Downloading certifi-2018.1.18-py2.py3-none-any.whl (151kB)
	Collecting chardet<3.1.0,>=3.0.2 (from requests>2.18.0->pipenv)
	Downloading chardet-3.0.4-py2.py3-none-any.whl (133kB)
	Collecting idna<2.7,>=2.5 (from requests>2.18.0->pipenv)
	Downloading idna-2.6-py2.py3-none-any.whl (56kB)
	Collecting pycodestyle<2.4.0,>=2.0.0 (from flake8>=3.0.0->pipenv)
	Downloading pycodestyle-2.3.1-py2.py3-none-any.whl (45kB)
	Collecting mccabe<0.7.0,>=0.6.0 (from flake8>=3.0.0->pipenv)
	Downloading mccabe-0.6.1-py2.py3-none-any.whl
	Collecting pyflakes<1.7.0,>=1.5.0 (from flake8>=3.0.0->pipenv)
	Downloading pyflakes-1.6.0-py2.py3-none-any.whl (227kB)
	Installing collected packages: virtualenv, virtualenv-clone, pew, certifi, chardet, idna, urllib3, requests, pycodestyle, mccabe, pyflakes, flake8, pipenv
	Running setup.py install for virtualenv-clone: started
	Running setup.py install for virtualenv-clone: finished with status 'done'
	Running setup.py install for pipenv: started
	Running setup.py install for pipenv: finished with status 'done'
	Successfully installed certifi-2018.1.18 chardet-3.0.4 flake8-3.5.0 idna-2.6 mccabe-0.6.1 pew-1.1.2 pipenv-9.0.3 pycodestyle-2.3.1 pyflakes-1.6.0 requests-2.18.4 urllib3-1.22 virtualenv-15.1.0 virtualenv-clone-0.2.6
	Removing intermediate container 222c51c8c187
	---> 9965dbca3f49
	Successfully built 9965dbca3f49
	Successfully tagged id3centos7:0.1.1
	SECURITY WARNING: You are building a Docker image from Windows against a non-Windows Docker host. All files and directories added to build context will have '-rwxr-xr-x' permissions. It is recommended to double check and reset permissions for sensitive files and directories.
	PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\centos7>



Nouveau fichier Dockerfile
============================


Dockerfile
------------

::

	# Use an official centos7 image
	FROM centos:7

	RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8
	ENV LANG fr_FR.utf8

	# gcc because we need regex and pyldap
	# openldap-devel because we need pyldap
	RUN yum update -y \
		&& yum install -y https://centos7.iuscommunity.org/ius-release.rpm \
		&& yum install -y python36u python36u-libs python36u-devel python36u-pip \
		&& yum install -y which gcc \
		&& yum install -y openldap-devel

	RUN python3.6 -m pip install pipenv

	WORKDIR /opt/intranet
	COPY Pipfile /opt/intranet/


Constuction de l'image docker build -t id3centos7:0.1.2 .
-------------------------------------------------------------

::

    PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\centos7> docker build -t id3centos7:0.1.2 .

::

	Sending build context to Docker daemon  195.1kB
	Step 1/7 : FROM centos:7
	 ---> ff426288ea90
	Step 2/7 : RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8
	 ---> Using cache
	 ---> b7dac1f044e3
	Step 3/7 : ENV LANG fr_FR.utf8
	 ---> Using cache
	 ---> e28a88050b8f
	Step 4/7 : RUN yum update -y     && yum install -y https://centos7.iuscommunity.org/ius-release.rpm     && yum install -y python36u python36u-libs python36u-devel python36u-pip     && yum install -y which gcc     && yum install -y openldap-devel
	 ---> Using cache
	 ---> 0cfdf4200049
	Step 5/7 : RUN python3.6 -m pip install pipenv
	 ---> Using cache
	 ---> 9965dbca3f49
	Step 6/7 : WORKDIR /opt/intranet
	Removing intermediate container ffc087754a0c
	 ---> aecca04b51f8
	Step 7/7 : COPY Pipfile /opt/intranet/
	 ---> e126ba1ca5f5
	Successfully built e126ba1ca5f5
	Successfully tagged id3centos7:0.1.2
	SECURITY WARNING: You are building a Docker image from Windows against a non-Windows Docker host. All files and directories added to build context will have '-rwxr-xr-x' permissions. It is recommended to double check and reset permissions for sensitive files and directories.


docker run --name id3centos7.1.2 -it id3centos7:0.1.2
-------------------------------------------------------

::

    PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\centos7> docker run --name id3centos7.1.2 -it id3centos7:0.1.2

::

	[root@8586df0dcb8e intranet]# pwd
	/opt/intranet


::

    [root@8586df0dcb8e intranet]# ls -als

::

	total 12
	4 drwxr-xr-x 1 root root 4096 févr.  2 13:43 .
	4 drwxr-xr-x 1 root root 4096 févr.  2 13:43 ..
	4 -rwxr-xr-x 1 root root  910 févr.  2 11:23 Pipfile



Problème : la commande pipenv


Nouveau dockerfile
====================

Dockerfile
-----------

::

	# Use an official centos7 image
	FROM centos:7

	RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8
	ENV LANG fr_FR.utf8

	# gcc because we need regex and pyldap
	# openldap-devel because we need pyldap
	RUN yum update -y \
		&& yum install -y https://centos7.iuscommunity.org/ius-release.rpm \
		&& yum install -y python36u python36u-libs python36u-devel python36u-pip \
		&& yum install -y which gcc \
		&& yum install -y openldap-devel

	RUN python3.6 -m pip install pipenv

	WORKDIR /opt/intranet

	# copy the Pipfile to the working directory
	COPY Pipfile /opt/intranet/
	# https://docs.pipenv.org/advanced/
	# This is useful for Docker containers, and deployment infrastructure (e.g. Heroku does this)
	RUN pipenv install



::

    PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\centos7> docker build -t id3centos7:0.1.3 .

::

	Sending build context to Docker daemon  198.1kB
	Step 1/8 : FROM centos:7
	 ---> ff426288ea90
	Step 2/8 : RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8
	 ---> Using cache
	 ---> b7dac1f044e3
	Step 3/8 : ENV LANG fr_FR.utf8
	 ---> Using cache
	 ---> e28a88050b8f
	Step 4/8 : RUN yum update -y     && yum install -y https://centos7.iuscommunity.org/ius-release.rpm     && yum install -y python36u python36u-libs python36u-devel python36u-pip     && yum install -y which gcc     && yum install -y openldap-devel
	 ---> Using cache
	 ---> 0cfdf4200049
	Step 5/8 : RUN python3.6 -m pip install pipenv
	 ---> Using cache
	 ---> 9965dbca3f49
	Step 6/8 : WORKDIR /opt/intranet
	 ---> Using cache
	 ---> aecca04b51f8
	Step 7/8 : COPY Pipfile /opt/intranet/
	 ---> Using cache
	 ---> 188cff4aa6e9
	Step 8/8 : RUN pipenv install
	 ---> Running in cdc65d965685
	Creating a virtualenv for this project…
	Using base prefix '/usr'
	New python executable in /root/.local/share/virtualenvs/intranet-6TUV_xiL/bin/python3.6
	Also creating executable in /root/.local/share/virtualenvs/intranet-6TUV_xiL/bin/python
	Installing setuptools, pip, wheel...done.

	Virtualenv location: /root/.local/share/virtualenvs/intranet-6TUV_xiL
	Pipfile.lock not found, creating…
	Locking [dev-packages] dependencies…
	Locking [packages] dependencies…
	Updated Pipfile.lock (326c76)!
	Installing dependencies from Pipfile.lock (326c76)…
	To activate this project's virtualenv, run the following:
	 $ pipenv shell
	Removing intermediate container cdc65d965685
	 ---> 179eac6f62c1
	Successfully built 179eac6f62c1
	Successfully tagged id3centos7:0.1.3
	SECURITY WARNING: You are building a Docker image from Windows against a non-Windows
	Docker host. All files and directories added to build context will have '-rwxr-xr-x' permissions.
	It is recommended to double check and reset permissions for sensitive files and directories.
	PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\centos7>



Nouveau fichier Dockerfile
============================

Dockerfile
-----------

::

	# Use an official centos7 image
	FROM centos:7

	RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8
	ENV LANG fr_FR.utf8

	# gcc because we need regex and pyldap
	# openldap-devel because we need pyldap
	RUN yum update -y \
		&& yum install -y https://centos7.iuscommunity.org/ius-release.rpm \
		&& yum install -y python36u python36u-libs python36u-devel python36u-pip \
		&& yum install -y which gcc \
		&& yum install -y openldap-devel

	RUN python3.6 -m pip install pipenv

	WORKDIR /opt/intranet

	# copy the Pipfile to the working directory
	ONBUILD COPY Pipfile /opt/intranet/
	# https://docs.pipenv.org/advanced/
	# https://github.com/pypa/pipenv/issues/1385
	# This is useful for Docker containers, and deployment infrastructure (e.g. Heroku does this)
	ONBUILD RUN pipenv install --system



::

    PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\centos7> docker build -t id3centos7:0.1.4 .

::

	Sending build context to Docker daemon  201.2kB
	Step 1/8 : FROM centos:7
	 ---> ff426288ea90
	Step 2/8 : RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8
	 ---> Using cache
	 ---> b7dac1f044e3
	Step 3/8 : ENV LANG fr_FR.utf8
	 ---> Using cache
	 ---> e28a88050b8f
	Step 4/8 : RUN yum update -y     && yum install -y https://centos7.iuscommunity.org/ius-release.rpm     && yum install -y python36u python36u-libs python36u-devel python36u-pip     && yum install -y which gcc     && yum install -y openldap-devel
	 ---> Using cache
	 ---> 0cfdf4200049
	Step 5/8 : RUN python3.6 -m pip install pipenv
	 ---> Using cache
	 ---> 9965dbca3f49
	Step 6/8 : WORKDIR /opt/intranet
	 ---> Using cache
	 ---> aecca04b51f8
	Step 7/8 : ONBUILD COPY Pipfile /opt/intranet/
	 ---> Running in 0d30cd780e8c
	Removing intermediate container 0d30cd780e8c
	 ---> c4a15216b54b
	Step 8/8 : ONBUILD RUN pipenv install --system
	 ---> Running in 9bb757ba3d15
	Removing intermediate container 9bb757ba3d15
	 ---> 237ec53f0462
	Successfully built 237ec53f0462
	Successfully tagged id3centos7:0.1.4
	SECURITY WARNING: You are building a Docker image from Windows against a non-Windows Docker host. All files and directories added to build context will have '-rwxr-xr-x' permissions. It is recommended to double check and reset permissions for sensitive files and directories.


Nouveau fichier Dockerfile
============================

::

    PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\centos7> docker build -t id3centos7:0.1.6 .

::

	Sending build context to Docker daemon  240.6kB
	Step 1/8 : FROM centos:7
	---> ff426288ea90
	Step 2/8 : RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8
	---> Using cache
	---> b7dac1f044e3
	Step 3/8 : ENV LANG fr_FR.utf8
	---> Using cache
	---> e28a88050b8f
	Step 4/8 : RUN yum update -y     && yum install -y https://centos7.iuscommunity.org/ius-release.rpm     && yum install -y python36u python36u-libs python36u-devel python36u-pip     && yum install -y which gcc     && yum install -y openldap-devel
	---> Using cache
	---> 0cfdf4200049
	Step 5/8 : RUN python3.6 -m pip install pipenv
	---> Using cache
	---> 9965dbca3f49
	Step 6/8 : WORKDIR /opt/intranet
	---> Using cache
	---> aecca04b51f8
	Step 7/8 : COPY requirements.txt /opt/intranet/
	---> 8ae3427dbfca
	Step 8/8 : RUN pip install -r requirements.txt
	---> Running in 555693a8d7bb
	/bin/sh: pip: command not found
	The command '/bin/sh -c pip install -r requirements.txt' returned a non-zero code: 127
	PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\centos7> docker build -t id3centos7:0.1.6 .
	Sending build context to Docker daemon  240.6kB
	Step 1/7 : FROM centos:7
	---> ff426288ea90
	Step 2/7 : RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8
	---> Using cache
	---> b7dac1f044e3
	Step 3/7 : ENV LANG fr_FR.utf8
	---> Using cache
	---> e28a88050b8f
	Step 4/7 : RUN yum update -y     && yum install -y https://centos7.iuscommunity.org/ius-release.rpm     && yum install -y python36u python36u-libs python36u-devel python36u-pip     && yum install -y which gcc     && yum install -y openldap-devel
	---> Using cache
	---> 0cfdf4200049
	Step 5/7 : WORKDIR /opt/intranet
	Removing intermediate container 2af4e31fb8ed
	---> 7fb09cc14c29
	Step 6/7 : COPY requirements.txt /opt/intranet/
	---> eecebec115f4
	Step 7/7 : RUN python3.6 -m pip install -r requirements.txt
	---> Running in 8400df97d2aa
	Collecting arrow==0.12.1 (from -r requirements.txt (line 1))
	Downloading arrow-0.12.1.tar.gz (65kB)
	Collecting babel==2.5.3 (from -r requirements.txt (line 2))
	Downloading Babel-2.5.3-py2.py3-none-any.whl (6.8MB)
	Collecting certifi==2018.1.18 (from -r requirements.txt (line 3))
	Downloading certifi-2018.1.18-py2.py3-none-any.whl (151kB)
	Collecting chardet==3.0.4 (from -r requirements.txt (line 4))
	Downloading chardet-3.0.4-py2.py3-none-any.whl (133kB)
	Collecting dateparser==0.6.0 (from -r requirements.txt (line 5))
	Downloading dateparser-0.6.0-py2.py3-none-any.whl (68kB)
	Collecting diff-match-patch==20121119 (from -r requirements.txt (line 6))
	Downloading diff-match-patch-20121119.tar.gz (54kB)
	Collecting django==2.0.2 (from -r requirements.txt (line 7))
	Downloading Django-2.0.2-py3-none-any.whl (7.1MB)
	Collecting django-ajax-selects==1.7.0 (from -r requirements.txt (line 8))
	Downloading django_ajax_selects-1.7.0-py3-none-any.whl
	Collecting django-autocomplete-light==3.2.10 (from -r requirements.txt (line 9))
	Downloading django-autocomplete-light-3.2.10.tar.gz (428kB)
	Collecting django-bootstrap4==0.0.5 (from -r requirements.txt (line 10))
	Downloading django-bootstrap4-0.0.5.tar.gz
	Collecting django-braces==1.12.0 (from -r requirements.txt (line 11))
	Downloading django_braces-1.12.0-py2.py3-none-any.whl
	Collecting django-countries==5.1.1 (from -r requirements.txt (line 12))
	Downloading django_countries-5.1.1-py2.py3-none-any.whl (682kB)
	Collecting django-crispy-forms==1.7.0 (from -r requirements.txt (line 13))
	Downloading django_crispy_forms-1.7.0-py2.py3-none-any.whl (104kB)
	Collecting django-embed-video==1.1.2 (from -r requirements.txt (line 14))
	Downloading django-embed-video-1.1.2.tar.gz
	Collecting django-environ==0.4.4 (from -r requirements.txt (line 15))
	Downloading django_environ-0.4.4-py2.py3-none-any.whl
	Collecting django-extended-choices==1.2 (from -r requirements.txt (line 16))
	Downloading django_extended_choices-1.2-py2.py3-none-any.whl
	Collecting django-extensions==1.9.9 (from -r requirements.txt (line 17))
	Downloading django_extensions-1.9.9-py2.py3-none-any.whl (213kB)
	Collecting django-import-export==0.7.0 (from -r requirements.txt (line 18))
	Downloading django_import_export-0.7.0-py2.py3-none-any.whl (72kB)
	Collecting django-localflavor==2.0 (from -r requirements.txt (line 19))
	Downloading django_localflavor-2.0-py2.py3-none-any.whl (2.4MB)
	Collecting django-money==0.12.3 (from -r requirements.txt (line 20))
	Downloading django_money-0.12.3-py2.py3-none-any.whl
	Collecting django-phonenumber-field==2.0.0 (from -r requirements.txt (line 21))
	Downloading django-phonenumber-field-2.0.0.tar.gz
	Collecting djangorestframework==3.7.7 (from -r requirements.txt (line 22))
	Downloading djangorestframework-3.7.7-py2.py3-none-any.whl (1.1MB)
	Collecting et-xmlfile==1.0.1 (from -r requirements.txt (line 23))
	Downloading et_xmlfile-1.0.1.tar.gz
	Collecting ftfy==5.3.0 (from -r requirements.txt (line 24))
	Downloading ftfy-5.3.0.tar.gz (53kB)
	Collecting future==0.16.0 (from -r requirements.txt (line 25))
	Downloading future-0.16.0.tar.gz (824kB)
	Collecting idna==2.6 (from -r requirements.txt (line 26))
	Downloading idna-2.6-py2.py3-none-any.whl (56kB)
	Collecting jdcal==1.3 (from -r requirements.txt (line 27))
	Downloading jdcal-1.3.tar.gz
	Collecting odfpy==1.3.6 (from -r requirements.txt (line 28))
	Downloading odfpy-1.3.6.tar.gz (691kB)
	Collecting openpyxl==2.5.0 (from -r requirements.txt (line 29))
	Downloading openpyxl-2.5.0.tar.gz (169kB)
	Collecting pendulum==1.4.0 (from -r requirements.txt (line 30))
	Downloading pendulum-1.4.0-cp36-cp36m-manylinux1_x86_64.whl (127kB)
	Collecting phonenumberslite==8.8.10 (from -r requirements.txt (line 31))
	Downloading phonenumberslite-8.8.10-py2.py3-none-any.whl (429kB)
	Collecting pillow==5.0.0 (from -r requirements.txt (line 32))
	Downloading Pillow-5.0.0-cp36-cp36m-manylinux1_x86_64.whl (5.9MB)
	Collecting prettytable==0.7.2 (from -r requirements.txt (line 33))
	Downloading prettytable-0.7.2.zip
	Collecting psycopg2==2.7.3.2 (from -r requirements.txt (line 34))
	Downloading psycopg2-2.7.3.2-cp36-cp36m-manylinux1_x86_64.whl (2.7MB)
	Collecting py-moneyed==0.7.0 (from -r requirements.txt (line 35))
	Downloading py_moneyed-0.7.0-py3-none-any.whl
	Collecting python-dateutil==2.6.1 (from -r requirements.txt (line 36))
	Downloading python_dateutil-2.6.1-py2.py3-none-any.whl (194kB)
	Collecting pytz==2017.3 (from -r requirements.txt (line 37))
	Downloading pytz-2017.3-py2.py3-none-any.whl (511kB)
	Collecting pytzdata==2018.3 (from -r requirements.txt (line 38))
	Downloading pytzdata-2018.3-py2.py3-none-any.whl (492kB)
	Collecting pyyaml==3.12 (from -r requirements.txt (line 39))
	Downloading PyYAML-3.12.tar.gz (253kB)
	Collecting regex==2018.1.10 (from -r requirements.txt (line 40))
	Downloading regex-2018.01.10.tar.gz (612kB)
	Collecting requests==2.18.4 (from -r requirements.txt (line 41))
	Downloading requests-2.18.4-py2.py3-none-any.whl (88kB)
	Collecting ruamel.yaml==0.15.35 (from -r requirements.txt (line 42))
	Downloading ruamel.yaml-0.15.35-cp36-cp36m-manylinux1_x86_64.whl (558kB)
	Collecting six==1.11.0 (from -r requirements.txt (line 43))
	Downloading six-1.11.0-py2.py3-none-any.whl
	Collecting sorl-thumbnail==12.4.1 (from -r requirements.txt (line 44))
	Downloading sorl_thumbnail-12.4.1-py2.py3-none-any.whl (44kB)
	Collecting sqlanydb==1.0.9 (from -r requirements.txt (line 45))
	Downloading sqlanydb-1.0.9.tar.gz
	Collecting tablib==0.12.1 (from -r requirements.txt (line 46))
	Downloading tablib-0.12.1.tar.gz (63kB)
	Collecting typing==3.6.4 (from -r requirements.txt (line 47))
	Downloading typing-3.6.4-py3-none-any.whl
	Collecting tzlocal==1.5.1 (from -r requirements.txt (line 48))
	Downloading tzlocal-1.5.1.tar.gz
	Collecting unicodecsv==0.14.1 (from -r requirements.txt (line 49))
	Downloading unicodecsv-0.14.1.tar.gz
	Collecting urllib3==1.22 (from -r requirements.txt (line 50))
	Downloading urllib3-1.22-py2.py3-none-any.whl (132kB)
	Collecting wcwidth==0.1.7 (from -r requirements.txt (line 51))
	Downloading wcwidth-0.1.7-py2.py3-none-any.whl
	Collecting xlrd==1.1.0 (from -r requirements.txt (line 52))
	Downloading xlrd-1.1.0-py2.py3-none-any.whl (108kB)
	Collecting xlwt==1.3.0 (from -r requirements.txt (line 53))
	Downloading xlwt-1.3.0-py2.py3-none-any.whl (99kB)
	Requirement already satisfied: setuptools in /usr/lib/python3.6/site-packages (from django-money==0.12.3->-r requirements.txt (line 20))
	Installing collected packages: six, python-dateutil, arrow, pytz, babel, certifi, chardet, regex, ruamel.yaml, tzlocal, dateparser, diff-match-patch, django, django-ajax-selects, django-autocomplete-light, django-bootstrap4, django-braces, django-countries, django-crispy-forms, idna, urllib3, requests, django-embed-video, django-environ, future, django-extended-choices, typing, django-extensions, odfpy, jdcal, et-xmlfile, openpyxl, unicodecsv, xlrd, xlwt, pyyaml, tablib, django-import-export, django-localflavor, py-moneyed, django-money, phonenumberslite, django-phonenumber-field, djangorestframework, wcwidth, ftfy, pytzdata, pendulum, pillow, prettytable, psycopg2, sorl-thumbnail, sqlanydb
	Running setup.py install for arrow: started
	Running setup.py install for arrow: finished with status 'done'
	Running setup.py install for regex: started
	Running setup.py install for regex: finished with status 'done'
	Running setup.py install for tzlocal: started
	Running setup.py install for tzlocal: finished with status 'done'
	Running setup.py install for diff-match-patch: started
	Running setup.py install for diff-match-patch: finished with status 'done'
	Running setup.py install for django-autocomplete-light: started
	Running setup.py install for django-autocomplete-light: finished with status 'done'
	Running setup.py install for django-bootstrap4: started
	Running setup.py install for django-bootstrap4: finished with status 'done'
	Running setup.py install for django-embed-video: started
	Running setup.py install for django-embed-video: finished with status 'done'
	Running setup.py install for future: started
	Running setup.py install for future: finished with status 'done'
	Running setup.py install for odfpy: started
	Running setup.py install for odfpy: finished with status 'done'
	Running setup.py install for jdcal: started
	Running setup.py install for jdcal: finished with status 'done'
	Running setup.py install for et-xmlfile: started
	Running setup.py install for et-xmlfile: finished with status 'done'
	Running setup.py install for openpyxl: started
	Running setup.py install for openpyxl: finished with status 'done'
	Running setup.py install for unicodecsv: started
	Running setup.py install for unicodecsv: finished with status 'done'
	Running setup.py install for pyyaml: started
	Running setup.py install for pyyaml: finished with status 'done'
	Running setup.py install for tablib: started
	Running setup.py install for tablib: finished with status 'done'
	Running setup.py install for django-phonenumber-field: started
	Running setup.py install for django-phonenumber-field: finished with status 'done'
	Running setup.py install for ftfy: started
	Running setup.py install for ftfy: finished with status 'done'
	Running setup.py install for prettytable: started
	Running setup.py install for prettytable: finished with status 'done'
	Running setup.py install for sqlanydb: started
	Running setup.py install for sqlanydb: finished with status 'done'
	Successfully installed arrow-0.12.1 babel-2.5.3 certifi-2018.1.18 chardet-3.0.4 dateparser-0.6.0 diff-match-patch-20121119 django-2.0.2 django-ajax-selects-1.7.0 django-autocomplete-light-3.2.10 django-bootstrap4-0.0.5 django-braces-1.12.0 django-countries-5.1.1 django-crispy-forms-1.7.0 django-embed-video-1.1.2 django-environ-0.4.4 django-extended-choices-1.2 django-extensions-1.9.9 django-import-export-0.7.0 django-localflavor-2.0 django-money-0.12.3 django-phonenumber-field-2.0.0 djangorestframework-3.7.7 et-xmlfile-1.0.1 ftfy-5.3.0 future-0.16.0 idna-2.6 jdcal-1.3 odfpy-1.3.6 openpyxl-2.5.0 pendulum-1.4.0 phonenumberslite-8.8.10 pillow-5.0.0 prettytable-0.7.2 psycopg2-2.7.3.2 py-moneyed-0.7.0 python-dateutil-2.6.1 pytz-2017.3 pytzdata-2018.3 pyyaml-3.12 regex-2018.1.10 requests-2.18.4 ruamel.yaml-0.15.35 six-1.11.0 sorl-thumbnail-12.4.1 sqlanydb-1.0.9 tablib-0.12.1 typing-3.6.4 tzlocal-1.5.1 unicodecsv-0.14.1 urllib3-1.22 wcwidth-0.1.7 xlrd-1.1.0 xlwt-1.3.0
	Removing intermediate container 8400df97d2aa
	---> bf91ebbc265a
	Successfully built bf91ebbc265a
	Successfully tagged id3centos7:0.1.6
	SECURITY WARNING: You are building a Docker image from Windows against a non-Windows Docker host. All files and directories added to build context will have '-rwxr-xr-x' permissions. It is recommended to double check and reset permissions for sensitive files and directories.
	PS Y:\projects_id3\P5N001\XLOGCA135_tutorial_docker\tutorial_docker\tutoriels\centos7>
