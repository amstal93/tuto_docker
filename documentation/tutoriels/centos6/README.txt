===========
README.txt
===========

.. seealso::

   - https://store.docker.com/images/centos


Lancer le conteneur
====================

::

    docker run --name centos6 -it centos:6


Accéder au conteneur
======================

::

    docker exec -it centos6 bash
