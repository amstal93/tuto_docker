

.. index::
   pair: Image ; rabbitmq
   pair: AMQP ; rabbitmq
   pair: celery ; rabbitmq
   pair: message-oriented ; middleware
   ! rabbitmq


.. _images_rabbitmq:

==============================
Images **rabbitmq**
==============================


.. seealso::

   - https://store.docker.com/images/rabbitmq
   - https://hub.docker.com/_/rabbitmq/
   - https://en.wikipedia.org/wiki/RabbitMQ





.. figure:: rabbitmq_logo.png
   :align: center

   Le logo de rabbitmq


What is RabbitMQ ?
====================

RabbitMQ is open source message broker software (sometimes called
message-oriented middleware) that implements the Advanced Message Queuing
Protocol (AMQP).

The RabbitMQ server is written in the Erlang programming language and
is built on the Open Telecom Platform framework for clustering and failover.
Client libraries to interface with the broker are available for all major
programming languages.


Rabbitmq and celery
=====================

.. seealso::

   - http://docs.celeryproject.org/en/latest/getting-started/brokers/rabbitmq.html
