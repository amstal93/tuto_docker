

.. index::
   pair: Image ; Hello-world
   ! Hello-world


.. _image_hello_world:

==============================
Image **hello world**
==============================


.. seealso::

   - https://hub.docker.com/_/hello-world/
   - https://store.docker.com/images/hello-world
   - https://github.com/docker/labs/blob/master/beginner/chapters/setup.md






.. figure:: images_hello_world.png
   :align: center

   https://hub.docker.com/_/hello-world/

Short Description
==================

Hello World! (an example of minimal Dockerization).
