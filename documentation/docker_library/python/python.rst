

.. index::
   pair: Images ; Python
   pair: 3.7 ; Python
   pair: 3.8 ; Python


.. _images_python:

==============================
Images **Python**
==============================


.. seealso::

   - https://store.docker.com/images/python
   - https://hub.docker.com/_/python/
   - :ref:`python_tuto`




.. figure:: Python_logo_and_wordmark.svg.png
   :align: center

   Le logo Python

.. figure:: images_python.png
   :align: center

   https://store.docker.com/images/python


Short Description
==================

Python is an interpreted, interactive, object-oriented, open-source
programming language.


What is Python ?
==================

Python is an interpreted, interactive, object-oriented, open-source
programming language.

It incorporates modules, exceptions, dynamic typing, very high level
dynamic data types, and classes. Python combines remarkable power with
very clear syntax.

It has interfaces to many system calls and libraries, as well as to
various window systems, and is extensible in C or C++.

It is also usable as an extension language for applications that need a
programmable interface.

Finally, Python is portable: it runs on many Unix variants, on the Mac,
and on Windows 2000 and later.


How to use this image
======================

Create a Dockerfile in your Python app project

::

    FROM python:3

    WORKDIR /usr/src/app

    COPY requirements.txt ./
    RUN pip install --no-cache-dir -r requirements.txt

    COPY . .

    CMD [ "python", "./your-daemon-or-script.py" ]

    You can then build and run the Docker image:

::

    docker build -t my-python-app .

::

    docker run -it --rm --name my-running-app my-python-app

Run a single Python script

For many simple, single file projects, you may find it inconvenient to
write a complete Dockerfile. In such cases, you can run a Python script
by using the Python Docker image directly:

::

    docker run -it --rm --name my-running-script -v "$PWD":/usr/src/myapp -w /usr/src/myapp python:3 python your-daemon-or-script.py



Tutorials
===========

.. toctree::
   :maxdepth: 3

   tutorials/tutorials


Versions
===========


erinxocon/pybuntu Python 3.8 (not for mod_wsgi)
-------------------------------------------------

.. seealso::

   - https://github.com/erinxocon/pybuntu-docker/blob/master/3.8.0/Dockerfile


.. warning:: not suitable for mod_wsgi because Python is not compiled
   with the --enable-shared option

   https://modwsgi.readthedocs.io/en/latest/user-guides/installation-issues.html

::

    FROM ubuntu:18.04

    ENV LC_ALL C.UTF-8
    ENV LANG C.UTF-8
    ENV PATH /usr/local/bin:$PATH
    ENV PYTHON_VERSION 3.8.0
    ENV ALPHA_VERSION a1
    ENV PYTHON_PIP_VERSION 19.0.2
    ENV BUILD_DEPS build-essential \
        libncursesw5-dev \
        libreadline-gplv2-dev \
        libssl-dev \
        libgdbm-dev \
        libc6-dev \
        libsqlite3-dev \
        libbz2-dev \
        ca-certificates \
        zlib1g-dev \
        wget \
        libffi-dev
    ENV REMOVE_DEPS build-essential \
        wget

    RUN apt update && apt install --no-install-recommends --no-install-suggests -y ${BUILD_DEPS} \
        && wget -O- https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}${ALPHA_VERSION}.tgz | tar xz \
        && cd Python-${PYTHON_VERSION}${ALPHA_VERSION}/ \
        && ./configure \
            --without-ensurepip \
            --enable-loadable-sqlite-extensions \
        && make -j "$(nproc)" \
        && make install \
        && find /usr/local -depth \
        \( \
            \( -type d -a \( -name test -o -name tests \) \) \
            -o \
            \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
        \) -exec rm -rf '{}' + \
        && rm -rf /Python-${PYTHON_VERSION}${ALPHA_VERSION} \
        \
        && python3 --version \
        \
        && cd /usr/local/bin \
        && ln -s pydoc3 pydoc \
        && ln -s python3 python \
        && cd \
        \
        set -ex; \
            \
            wget -O get-pip.py 'https://bootstrap.pypa.io/get-pip.py'; \
            \
            python get-pip.py \
                --disable-pip-version-check \
                --cache-dir=/pipcache \
                "pip==$PYTHON_PIP_VERSION" \
            ; \
            pip --version; \
            \
            find /usr/local -depth \
                \( \
                    \( -type d -a \( -name test -o -name tests \) \) \
                    -o \
                    \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
                \) -exec rm -rf '{}' +; \
        rm -f get-pip.py \
        && rm -rf /pipcache \
        && apt remove --autoremove --purge -y ${REMOVE_DEPS} \
        && rm -rf /var/lib/apt/lists/*

    CMD ["python"]


::

    docker pull jetblackpope/pybuntu:3.8

::

    3.8: Pulling from jetblackpope/pybuntu
    6cf436f81810: Already exists
    987088a85b96: Already exists
    b4624b3efe06: Already exists
    d42beb8ded59: Already exists
    522f421070c0: Pull complete
    Digest: sha256:f23a99cac7807646b90fe071ea77f09a095aa13431e71046192fe71db94aa292
    Status: Downloaded newer image for jetblackpope/pybuntu:3.8


::

    pvergain@UC004 > docker images

::

    REPOSITORY                            TAG                 IMAGE ID            CREATED             SIZE
    docker.id3.eu/informatique/log        latest              3d945b383b70        2 hours ago         855MB
    jetblackpope/pybuntu                  3.8                 56a601adcbd4        15 hours ago        253MB




3.8.0-dev, erinxocon/pybuntu-docker (not for mod_wsgi)
---------------------------------------------------------

.. seealso:

   - https://raw.githubusercontent.com/erinxocon/pybuntu-docker/master/3.8.0-dev/Dockerfile


.. warning:: not suitable for mod_wsgi because Python is not compiled
   with the --enable-shared option

   https://modwsgi.readthedocs.io/en/latest/user-guides/installation-issues.html

::

    FROM ubuntu:18.04

    ENV LC_ALL C.UTF-8
    ENV LANG C.UTF-8
    ENV PATH /usr/local/bin:$PATH
    ENV PYTHON_VERSION 3.8.0
    ENV ALPHA_VERSION a1
    ENV PYTHON_PIP_VERSION 19.0.2
    ENV BUILD_DEPS build-essential \
        libncursesw5-dev \
        libreadline-gplv2-dev \
        libssl-dev \
        libgdbm-dev \
        libc6-dev \
        libsqlite3-dev \
        libbz2-dev \
        ca-certificates \
        zlib1g-dev \
        wget \
        curl \
        libffi-dev
    ENV REMOVE_DEPS build-essential \
        wget

    RUN apt update && apt install --no-install-recommends --no-install-suggests -y ${BUILD_DEPS} \
        && wget -O- https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}${ALPHA_VERSION}.tgz | tar xz \
        && cd Python-${PYTHON_VERSION}${ALPHA_VERSION}/ \
        && ./configure \
            --without-ensurepip \
            --enable-loadable-sqlite-extensions \
        && make -j "$(nproc)" \
        && make install \
        && find /usr/local -depth \
        \( \
            \( -type d -a \( -name test -o -name tests \) \) \
            -o \
            \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
        \) -exec rm -rf '{}' + \
        && rm -rf /Python-${PYTHON_VERSION}${ALPHA_VERSION} \
        \
        && python3 --version \
        \
        && cd /usr/local/bin \
        && ln -s pydoc3 pydoc \
        && ln -s python3 python \
        && cd \
        \
        set -ex; \
            \
            wget -O get-pip.py 'https://bootstrap.pypa.io/get-pip.py'; \
            \
            python get-pip.py \
                --disable-pip-version-check \
                --cache-dir=/pipcache \
                "pip==$PYTHON_PIP_VERSION" \
            ; \
            pip --version; \
            \
            find /usr/local -depth \
                \( \
                    \( -type d -a \( -name test -o -name tests \) \) \
                    -o \
                    \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
                \) -exec rm -rf '{}' +; \
        rm -f get-pip.py \
        && rm -rf /pipcache

        CMD ["python"]



erinxocon/pybuntu Python 3.7 (237MB)
--------------------------------------

.. seealso::

   - https://github.com/erinxocon/pybuntu-docker/blob/master/3.7.2/Dockerfile

.. warning:: not suitable for mod_wsgi because Python is not compiled
   with the --enable-shared option

   https://modwsgi.readthedocs.io/en/latest/user-guides/installation-issues.html


::

    FROM ubuntu:18.04

    ENV LC_ALL C.UTF-8
    ENV LANG C.UTF-8
    ENV PATH /usr/local/bin:$PATH
    ENV PYTHON_VERSION 3.7.2
    ENV PYTHON_PIP_VERSION 19.0.2
    ENV BUILD_DEPS build-essential \
        libncursesw5-dev \
        libreadline-gplv2-dev \
        libssl-dev \
        libgdbm-dev \
        libc6-dev \
        libsqlite3-dev \
        libbz2-dev \
        ca-certificates \
        zlib1g-dev \
        wget \
        libffi-dev
    ENV REMOVE_DEPS build-essential \
        wget

    RUN apt update && apt install --no-install-recommends --no-install-suggests -y ${BUILD_DEPS} \
        && wget -O- https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz | tar xz \
        && cd Python-${PYTHON_VERSION}/ \
        && ./configure \
            --without-ensurepip \
            --enable-loadable-sqlite-extensions \
        && make -j "$(nproc)" \
        && make install \
        && find /usr/local -depth \
        \( \
            \( -type d -a \( -name test -o -name tests \) \) \
            -o \
            \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
        \) -exec rm -rf '{}' + \
        && rm -rf /Python-${PYTHON_VERSION} \
        \
        && python3 --version \
        \
        && cd /usr/local/bin \
        && ln -s pydoc3 pydoc \
        && ln -s python3 python \
        && cd \
        \
        set -ex; \
            \
            wget -O get-pip.py 'https://bootstrap.pypa.io/get-pip.py'; \
            \
            python get-pip.py \
                --disable-pip-version-check \
                --cache-dir=/pipcache \
                "pip==$PYTHON_PIP_VERSION" \
            ; \
            pip --version; \
            \
            find /usr/local -depth \
                \( \
                    \( -type d -a \( -name test -o -name tests \) \) \
                    -o \
                    \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
                \) -exec rm -rf '{}' +; \
        rm -f get-pip.py \
        && rm -rf /pipcache \
        && apt remove --autoremove --purge -y ${REMOVE_DEPS} \
        && rm -rf /var/lib/apt/lists/*

    CMD ["python"]


::

     pvergain@UC004 > docker pull jetblackpope/pybuntu:3.7

::

    3.7: Pulling from jetblackpope/pybuntu
    6cf436f81810: Pull complete
    987088a85b96: Pull complete
    b4624b3efe06: Pull complete
    d42beb8ded59: Pull complete
    bca0adb5e7c3: Pull complete
    Digest: sha256:2de022423a0f3ce94c961177fc5b96c4638b3905cdcc1bd16dc3072baabcb5fe
    Status: Downloaded newer image for jetblackpope/pybuntu:3.7

::

     pvergain@UC004 > docker images

::

    REPOSITORY                            TAG                 IMAGE ID            CREATED             SIZE
    jetblackpope/pybuntu                  3.7                 a6c0ab4183ec        13 hours ago        237MB




3.7.2 Debian, strech
----------------------

.. seealso::

   - https://github.com/docker-library/python/blob/344fd4f05b1e81dd97f8334e30035c0359dfde7f/3.7/stretch/Dockerfile


::

    #
    # NOTE: THIS DOCKERFILE IS GENERATED VIA "update.sh"
    #
    # PLEASE DO NOT EDIT IT DIRECTLY.
    #

    FROM buildpack-deps:stretch

    # ensure local python is preferred over distribution python
    ENV PATH /usr/local/bin:$PATH

    # http://bugs.python.org/issue19846
    # > At the moment, setting "LANG=C" on a Linux system *fundamentally breaks Python 3*, and that's not OK.
    ENV LANG C.UTF-8

    # extra dependencies (over what buildpack-deps already includes)
    RUN apt-get update && apt-get install -y --no-install-recommends \
            tk-dev \
            uuid-dev \
        && rm -rf /var/lib/apt/lists/*

    ENV GPG_KEY 0D96DF4D4110E5C43FBFB17F2D347EA6AA65421D
    ENV PYTHON_VERSION 3.7.2

    RUN set -ex \
        \
        && wget -O python.tar.xz "https://www.python.org/ftp/python/${PYTHON_VERSION%%[a-z]*}/Python-$PYTHON_VERSION.tar.xz" \
        && wget -O python.tar.xz.asc "https://www.python.org/ftp/python/${PYTHON_VERSION%%[a-z]*}/Python-$PYTHON_VERSION.tar.xz.asc" \
        && export GNUPGHOME="$(mktemp -d)" \
        && gpg --batch --keyserver ha.pool.sks-keyservers.net --recv-keys "$GPG_KEY" \
        && gpg --batch --verify python.tar.xz.asc python.tar.xz \
        && { command -v gpgconf > /dev/null && gpgconf --kill all || :; } \
        && rm -rf "$GNUPGHOME" python.tar.xz.asc \
        && mkdir -p /usr/src/python \
        && tar -xJC /usr/src/python --strip-components=1 -f python.tar.xz \
        && rm python.tar.xz \
        \
        && cd /usr/src/python \
        && gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)" \
        && ./configure \
            --build="$gnuArch" \
            --enable-loadable-sqlite-extensions \
            --enable-shared \
            --with-system-expat \
            --with-system-ffi \
            --without-ensurepip \
        && make -j "$(nproc)" \
        && make install \
        && ldconfig \
        \
        && find /usr/local -depth \
            \( \
                \( -type d -a \( -name test -o -name tests \) \) \
                -o \
                \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
            \) -exec rm -rf '{}' + \
        && rm -rf /usr/src/python \
        \
        && python3 --version

    # make some useful symlinks that are expected to exist
    RUN cd /usr/local/bin \
        && ln -s idle3 idle \
        && ln -s pydoc3 pydoc \
        && ln -s python3 python \
        && ln -s python3-config python-config

    # if this is called "PIP_VERSION", pip explodes with "ValueError: invalid truth value '<VERSION>'"
    ENV PYTHON_PIP_VERSION 19.0.1

    RUN set -ex; \
        \
        wget -O get-pip.py 'https://bootstrap.pypa.io/get-pip.py'; \
        \
        python get-pip.py \
            --disable-pip-version-check \
            --no-cache-dir \
            "pip==$PYTHON_PIP_VERSION" \
        ; \
        pip --version; \
        \
        find /usr/local -depth \
            \( \
                \( -type d -a \( -name test -o -name tests \) \) \
                -o \
                \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
            \) -exec rm -rf '{}' +; \
        rm -f get-pip.py

    CMD ["python3"]



3.7.2 strech-slim
--------------------

.. seealso::

    https://github.com/docker-library/python/blob/344fd4f05b1e81dd97f8334e30035c0359dfde7f/3.7/stretch/slim/Dockerfile


::

    #
    # NOTE: THIS DOCKERFILE IS GENERATED VIA "update.sh"
    #
    # PLEASE DO NOT EDIT IT DIRECTLY.
    #

    FROM debian:stretch-slim

    # ensure local python is preferred over distribution python
    ENV PATH /usr/local/bin:$PATH

    # http://bugs.python.org/issue19846
    # > At the moment, setting "LANG=C" on a Linux system *fundamentally breaks Python 3*, and that's not OK.
    ENV LANG C.UTF-8

    # runtime dependencies
    RUN apt-get update && apt-get install -y --no-install-recommends \
            ca-certificates \
            netbase \
        && rm -rf /var/lib/apt/lists/*

    ENV GPG_KEY 0D96DF4D4110E5C43FBFB17F2D347EA6AA65421D
    ENV PYTHON_VERSION 3.7.2

    RUN set -ex \
        \
        && savedAptMark="$(apt-mark showmanual)" \
        && apt-get update && apt-get install -y --no-install-recommends \
            dpkg-dev \
            gcc \
            libbz2-dev \
            libc6-dev \
            libexpat1-dev \
            libffi-dev \
            libgdbm-dev \
            liblzma-dev \
            libncursesw5-dev \
            libreadline-dev \
            libsqlite3-dev \
            libssl-dev \
            make \
            tk-dev \
            uuid-dev \
            wget \
            xz-utils \
            zlib1g-dev \
    # as of Stretch, "gpg" is no longer included by default
            $(command -v gpg > /dev/null || echo 'gnupg dirmngr') \
        \
        && wget -O python.tar.xz "https://www.python.org/ftp/python/${PYTHON_VERSION%%[a-z]*}/Python-$PYTHON_VERSION.tar.xz" \
        && wget -O python.tar.xz.asc "https://www.python.org/ftp/python/${PYTHON_VERSION%%[a-z]*}/Python-$PYTHON_VERSION.tar.xz.asc" \
        && export GNUPGHOME="$(mktemp -d)" \
        && gpg --batch --keyserver ha.pool.sks-keyservers.net --recv-keys "$GPG_KEY" \
        && gpg --batch --verify python.tar.xz.asc python.tar.xz \
        && { command -v gpgconf > /dev/null && gpgconf --kill all || :; } \
        && rm -rf "$GNUPGHOME" python.tar.xz.asc \
        && mkdir -p /usr/src/python \
        && tar -xJC /usr/src/python --strip-components=1 -f python.tar.xz \
        && rm python.tar.xz \
        \
        && cd /usr/src/python \
        && gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)" \
        && ./configure \
            --build="$gnuArch" \
            --enable-loadable-sqlite-extensions \
            --enable-shared \
            --with-system-expat \
            --with-system-ffi \
            --without-ensurepip \
        && make -j "$(nproc)" \
        && make install \
        && ldconfig \
        \
        && apt-mark auto '.*' > /dev/null \
        && apt-mark manual $savedAptMark \
        && find /usr/local -type f -executable -not \( -name '*tkinter*' \) -exec ldd '{}' ';' \
            | awk '/=>/ { print $(NF-1) }' \
            | sort -u \
            | xargs -r dpkg-query --search \
            | cut -d: -f1 \
            | sort -u \
            | xargs -r apt-mark manual \
        && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
        && rm -rf /var/lib/apt/lists/* \
        \
        && find /usr/local -depth \
            \( \
                \( -type d -a \( -name test -o -name tests \) \) \
                -o \
                \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
            \) -exec rm -rf '{}' + \
        && rm -rf /usr/src/python \
        \
        && python3 --version

    # make some useful symlinks that are expected to exist
    RUN cd /usr/local/bin \
        && ln -s idle3 idle \
        && ln -s pydoc3 pydoc \
        && ln -s python3 python \
        && ln -s python3-config python-config

    # if this is called "PIP_VERSION", pip explodes with "ValueError: invalid truth value '<VERSION>'"
    ENV PYTHON_PIP_VERSION 19.0.1

    RUN set -ex; \
        \
        savedAptMark="$(apt-mark showmanual)"; \
        apt-get update; \
        apt-get install -y --no-install-recommends wget; \
        \
        wget -O get-pip.py 'https://bootstrap.pypa.io/get-pip.py'; \
        \
        apt-mark auto '.*' > /dev/null; \
        [ -z "$savedAptMark" ] || apt-mark manual $savedAptMark; \
        apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
        rm -rf /var/lib/apt/lists/*; \
        \
        python get-pip.py \
            --disable-pip-version-check \
            --no-cache-dir \
            "pip==$PYTHON_PIP_VERSION" \
        ; \
        pip --version; \
        \
        find /usr/local -depth \
            \( \
                \( -type d -a \( -name test -o -name tests \) \) \
                -o \
                \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
            \) -exec rm -rf '{}' +; \
        rm -f get-pip.py

    CMD ["python3"]



3.7.2 alpine 3.9
-------------------


.. seealso::

   - https://raw.githubusercontent.com/docker-library/python/662514b58264cff440912d93648ecf202a43f31c/3.7/alpine3.9/Dockerfile


::

    #
    # NOTE: THIS DOCKERFILE IS GENERATED VIA "update.sh"
    #
    # PLEASE DO NOT EDIT IT DIRECTLY.
    #

    FROM alpine:3.9

    # ensure local python is preferred over distribution python
    ENV PATH /usr/local/bin:$PATH

    # http://bugs.python.org/issue19846
    # > At the moment, setting "LANG=C" on a Linux system *fundamentally breaks Python 3*, and that's not OK.
    ENV LANG C.UTF-8

    # install ca-certificates so that HTTPS works consistently
    # other runtime dependencies for Python are installed later
    RUN apk add --no-cache ca-certificates

    ENV GPG_KEY 0D96DF4D4110E5C43FBFB17F2D347EA6AA65421D
    ENV PYTHON_VERSION 3.7.2

    RUN set -ex \
        && apk add --no-cache --virtual .fetch-deps \
            gnupg \
            tar \
            xz \
        \
        && wget -O python.tar.xz "https://www.python.org/ftp/python/${PYTHON_VERSION%%[a-z]*}/Python-$PYTHON_VERSION.tar.xz" \
        && wget -O python.tar.xz.asc "https://www.python.org/ftp/python/${PYTHON_VERSION%%[a-z]*}/Python-$PYTHON_VERSION.tar.xz.asc" \
        && export GNUPGHOME="$(mktemp -d)" \
        && gpg --batch --keyserver ha.pool.sks-keyservers.net --recv-keys "$GPG_KEY" \
        && gpg --batch --verify python.tar.xz.asc python.tar.xz \
        && { command -v gpgconf > /dev/null && gpgconf --kill all || :; } \
        && rm -rf "$GNUPGHOME" python.tar.xz.asc \
        && mkdir -p /usr/src/python \
        && tar -xJC /usr/src/python --strip-components=1 -f python.tar.xz \
        && rm python.tar.xz \
        \
        && apk add --no-cache --virtual .build-deps  \
            bzip2-dev \
            coreutils \
            dpkg-dev dpkg \
            expat-dev \
            findutils \
            gcc \
            gdbm-dev \
            libc-dev \
            libffi-dev \
            libnsl-dev \
            libtirpc-dev \
            linux-headers \
            make \
            ncurses-dev \
            openssl-dev \
            pax-utils \
            readline-dev \
            sqlite-dev \
            tcl-dev \
            tk \
            tk-dev \
            util-linux-dev \
            xz-dev \
            zlib-dev \
    # add build deps before removing fetch deps in case there's overlap
        && apk del .fetch-deps \
        \
        && cd /usr/src/python \
        && gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)" \
        && ./configure \
            --build="$gnuArch" \
            --enable-loadable-sqlite-extensions \
            --enable-shared \
            --with-system-expat \
            --with-system-ffi \
            --without-ensurepip \
        && make -j "$(nproc)" \
    # set thread stack size to 1MB so we don't segfault before we hit sys.getrecursionlimit()
    # https://github.com/alpinelinux/aports/commit/2026e1259422d4e0cf92391ca2d3844356c649d0
            EXTRA_CFLAGS="-DTHREAD_STACK_SIZE=0x100000" \
        && make install \
        \
        && find /usr/local -type f -executable -not \( -name '*tkinter*' \) -exec scanelf --needed --nobanner --format '%n#p' '{}' ';' \
            | tr ',' '\n' \
            | sort -u \
            | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
            | xargs -rt apk add --no-cache --virtual .python-rundeps \
        && apk del .build-deps \
        \
        && find /usr/local -depth \
            \( \
                \( -type d -a \( -name test -o -name tests \) \) \
                -o \
                \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
            \) -exec rm -rf '{}' + \
        && rm -rf /usr/src/python \
        \
        && python3 --version

    # make some useful symlinks that are expected to exist
    RUN cd /usr/local/bin \
        && ln -s idle3 idle \
        && ln -s pydoc3 pydoc \
        && ln -s python3 python \
        && ln -s python3-config python-config

    # if this is called "PIP_VERSION", pip explodes with "ValueError: invalid truth value '<VERSION>'"
    ENV PYTHON_PIP_VERSION 19.0.1

    RUN set -ex; \
        \
        wget -O get-pip.py 'https://bootstrap.pypa.io/get-pip.py'; \
        \
        python get-pip.py \
            --disable-pip-version-check \
            --no-cache-dir \
            "pip==$PYTHON_PIP_VERSION" \
        ; \
        pip --version; \
        \
        find /usr/local -depth \
            \( \
                \( -type d -a \( -name test -o -name tests \) \) \
                -o \
                \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
            \) -exec rm -rf '{}' +; \
        rm -f get-pip.py

    CMD ["python3"]
