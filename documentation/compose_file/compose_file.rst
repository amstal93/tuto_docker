.. index::
   ! compose-file
   pair: compose-file ; 3.6

.. _compose_file:

==============================
compose-file
==============================


.. seealso::

   - https://docs.docker.com/compose/overview
   - https://docs.docker.com/compose/reference
   - https://docs.docker.com/compose/compose-file
   - https://docs.docker.com/compose/samples-for-compose
   - https://github.com/search?q=in%3Apath+docker-compose.yml+extension%3Ayml&type=Code
   - :ref:`docker_compose_versions`




Versions
==========

.. seealso:: https://docs.docker.com/compose/compose-file/compose-versioning


3.7
----

.. seealso::

   - https://docs.docker.com/compose/compose-file/compose-versioning/#version-37


An upgrade of version 3 that introduces new parameters.

It is only available with Docker Engine version :ref:`18.06.0 <docker_18_06_0>`
and higher.

Introduces the following additional parameters:

- init in service definitions
- rollback_config in deploy configurations
- Support for extension fields at the root of service, network, volume,
  secret and config definitions


3.6
----

.. seealso::

   - https://docs.docker.com/compose/compose-file/compose-versioning/#version-36


An upgrade of version 3 that introduces new parameters.
It is only available with Docker Engine version 18.02.0 and higher.

Introduces the following additional parameters::

    tmpfs size for tmpfs-type mounts
