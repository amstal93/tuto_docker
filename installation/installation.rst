.. index::
   pair: Installation ; Docker


.. _docker_install:

======================
Installation
======================


.. seealso::

   - https://docs.docker.com/install
